﻿namespace RPG.Core
{
    using RPG.Abilities;
    using RPG.Inventory;
    using RPG.Items;
    using UnityEngine;

    [System.Serializable]
    public class CharacterStat
    {
        public Stat statType;
        public int value;
    }

    [CreateAssetMenu(menuName = "CharacterClass/New")]
    public class CharacterClass : ScriptableObject, StatContainer
    {
        [SerializeField] private string className;
        [TextArea(3, 6)]
        [SerializeField] private string classDesc;

        [SerializeField] private CharacterStat[] stats;
        [SerializeField] private Item[] startingItems;
        [SerializeField] private AbilityBase[] startingAbilities; 



        public Item[] GetStartingItems()
        {
            Item[] items = new Item[startingItems.Length];

            for (int i = 0; i < items.Length; i++)
            {
                //items[i] = ItemUtility.CreateItem(startingItems[i]);
            }

            return items;
        }

        public AbilityBase[] GetStartingAbilities()
        {
            return startingAbilities;
        }

        public CharacterStat[] GetStats()
        {
            return stats;
        }
    }
}