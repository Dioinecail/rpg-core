﻿namespace RPG.Core
{
    using System.Collections.Generic;
    using System.IO;
    using UnityEngine;

    public static class PlayerPrefsManager
    {
        private const string itemPrefFileName = "item-prefs.items";
        private const string itemPrefsCountName = "item-prefs-count";

        private static List<string> itemKeys = new List<string>();



        public static void SaveItemPref(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
            PlayerPrefs.Save();

            itemKeys.Add(key);

            SaveItemPrefs();
        }

        public static bool GetItemPref(string key)
        {
            return PlayerPrefs.GetInt(key, 0) == 1;
        }

        public static void ResetItemPrefs()
        {
            LoadItemPrefs();

            for (int i = 0; i < itemKeys.Count; i++)
            {
                PlayerPrefs.DeleteKey(itemKeys[i]);
            }

            PlayerPrefs.Save();

            DeleteItemPrefs();
        }

        public static void ResetAllPrefs()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();

            DeleteItemPrefs();
        }

        #region FILE IO

        private static void LoadItemPrefs()
        {
            string path = Path.Combine(Application.persistentDataPath, itemPrefFileName);

            using (BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Create)))
            {
                itemKeys = new List<string>();

                int itemsCount = PlayerPrefs.GetInt(itemPrefsCountName, 0);

                for (int i = 0; i < itemsCount; i++)
                {
                    itemKeys.Add(reader.ReadString());
                }

                reader.Dispose();
                reader.Close();
            }
        }

        private static void SaveItemPrefs()
        {
            string path = Path.Combine(Application.persistentDataPath, itemPrefFileName);

            using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create)))
            {
                for (int i = 0; i < itemKeys.Count; i++)
                {
                    writer.Write(itemKeys[i]);
                }

                writer.Flush();
                writer.Close();
            }
        }

        private static void DeleteItemPrefs()
        {
            string path = Path.Combine(Application.persistentDataPath, itemPrefFileName);

            if(File.Exists(path))
            {
                File.Delete(path);
            }
        }

        #endregion
    }
}