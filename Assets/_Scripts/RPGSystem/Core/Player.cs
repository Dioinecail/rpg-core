﻿namespace RPG.Core
{
    using System;
    using System.Collections.Generic;
    using RPG.Damage;
    using RPG.Equipment;
    using RPG.Inventory;
    using RPG.Abilities;
    using UnityEngine;
    using RPG.Items;

    public enum Stat
    {
        STR = 0,
        DEX = 1,
        INT = 2,
        SPD = 7,
        VIT = 3,
        LCK = 4,
        DEF = 5,
        MDF = 6
    }

    /// <summary>
    /// Class that contains base stats of currently inited character
    /// </summary>
    public class Player : GameEntity
    {
        public PlayerEquipment Equipment { get; private set; }
        public PlayerInventory Inventory { get; private set; }
        public PlayerAbilities Abilities { get; private set; }



        public void Init(CharacterClass playerClass)
        {
            Inventory = new PlayerInventory(playerClass);
            Equipment = new PlayerEquipment(playerClass);
            Abilities = new PlayerAbilities(playerClass);

            for (int x = 0; x < Inventory.InventorySlots.GetLength(0); x++)
            {
                for (int y = 0; y < Inventory.InventorySlots.GetLength(1); y++)
                {
                    if (Inventory.InventorySlots[x, y].IsTaken && Inventory.InventorySlots[x, y].item.Equip != null)
                    {
                        Inventory.InventorySlots[x, y].item.Equip.Perform();
                    }
                }
            }

            statsDictionry = new Dictionary<Stat, int>();
            modifiers = new Dictionary<Stat, List<int>>();

            stats = playerClass.GetStats();

            Init();
        }

        public int GetBaseStat(Stat stat)
        {
            return statsDictionry[stat];
        }

        public int GetTotalStat(Stat stat)
        {
            int playerBaseStat = GetBaseStat(stat);
            int modifierAmount = 0;

            if (modifiers.ContainsKey(stat))
                modifierAmount = GetTotalModifier(stat);

            return playerBaseStat + modifierAmount;
        }

        public int GetDamage(EquipCategory category, Stat modifier)
        {
            int modifierStat = GetTotalStat(modifier);
            int minDamage = 0;
            int maxDamage = 0;

            Item weapon = Equipment.GetItem(category);

            //if (weapon.TargetItem is EquipItem)
            //{
            //    WeaponItem wItem = weapon.ItemReferense as WeaponItem;
            //    minDamage = wItem.GetMinDamage() + modifierStat;
            //    maxDamage = wItem.GetMaxDamage() + modifierStat;
            //}

            return UnityEngine.Random.Range(minDamage, maxDamage + 1);
        }

        public void AddModifier(Stat stat, int value)
        {
            if (modifiers.ContainsKey(stat))
            {
                modifiers[stat].Add(value);
            }
            else
            {
                modifiers.Add(stat, new List<int> { value });
            }
        }

        public void RemoveModifier(Stat stat, int value)
        {
            if (modifiers.ContainsKey(stat))
            {
                modifiers[stat].Remove(value);

                if (modifiers[stat].Count == 0)
                    modifiers.Remove(stat);
            }
            else
                Debug.LogError("Trying to remove a modifier that doesn't exist on entity!");
        }

        public int GetTotalModifier(Stat stat)
        {
            int value = 0;

            if (modifiers.ContainsKey(stat))
            {
                var m = modifiers[stat];

                for (int i = 0; i < m.Count; i++)
                {
                    value += m[i];
                }
            }

            return value;
        }

        public override CharacterStat[] GetStats()
        {
            Dictionary<Stat, int>.KeyCollection keys = statsDictionry.Keys;
            CharacterStat[] statContainer = new CharacterStat[keys.Count];
            int index = 0;

            foreach (var key in keys)
            {
                statContainer[index] = new CharacterStat();
                statContainer[index].statType = key;
                statContainer[index].value = GetTotalStat(key);
                index++;
            }

            return statContainer;
        }
    }
}