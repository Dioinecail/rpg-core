﻿namespace RPG.Core
{
    public interface StatContainer
    {
        CharacterStat[] GetStats();
    }
}