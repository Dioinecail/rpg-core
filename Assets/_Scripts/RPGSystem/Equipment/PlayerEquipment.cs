﻿namespace RPG.Equipment
{
    using System;
    using System.Collections.Generic;
    using RPG.Core;
    using RPG.Items;

    public enum EquipCategory
    {
        WEAPON,
        HEADGEAR,
        ARMOR,
        BOOTS,
        GLOVES,
        RING,
        AMULET,
        BELT
    }

    public class ItemEquippedEventArgs
    {
        public Item itemEquipped;
        public Item itemUnequipped;
    }

    public class PlayerEquipment
    {
        public event Action<ItemEquippedEventArgs> onItemEquipped;

        private Dictionary<EquipCategory, Item> equipment;



        public PlayerEquipment(CharacterClass playerClass)
        {
            equipment = new Dictionary<EquipCategory, Item>();

            var allStats = Enum.GetNames(typeof(EquipCategory));

            for (int i = 0; i < allStats.Length; i++)
            {
                EquipCategory selected;
                if (Enum.TryParse(allStats[i], out selected))
                {
                    equipment.Add(selected, null);
                }
            }
        }

        public bool EquipItem(Item newItem, EquipCategory category)
        {
            // check if item can be equipped at all

            if (!IsEquippable(newItem))
                return false;

            if(equipment[category] != null && equipment[category].Equals(newItem))
            {
                UnequipItem(category);
            }
            else
            {
                ItemEquippedEventArgs e = new ItemEquippedEventArgs();
                e.itemUnequipped = equipment[category];
                e.itemEquipped = newItem;

                equipment[category] = newItem;

                if (!newItem.IsIdentified)
                    newItem.Identify();

                onItemEquipped?.Invoke(e);
            }

            return true;
        }

        public bool UnequipItem(EquipCategory category)
        {
            var item = equipment[category];

            if (item.IsCursed)
                return false;

            ItemEquippedEventArgs e = new ItemEquippedEventArgs();
            e.itemUnequipped = equipment[category];

            equipment[category] = null;

            onItemEquipped?.Invoke(e);

            return true;
        }

        public Item GetItem(EquipCategory category)
        {
            if (equipment.ContainsKey(category))
                return equipment[category];

            return null;
        }

        private bool IsEquippable(Item newItem)
        {
            throw new NotImplementedException();
        }
    }
}