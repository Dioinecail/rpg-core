﻿namespace RPG.Inventory
{
    using RPG.Items;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class Drop
    {
        public int targetItemId;
        [Range(0, 101)] public ushort dropChance;
    }

    [CreateAssetMenu(menuName = "Items/DropItemSet", fileName = "ItemSet_")]
    public class DropItemSet : ScriptableObject
    {
        public Drop[] targetItems;
    }
}