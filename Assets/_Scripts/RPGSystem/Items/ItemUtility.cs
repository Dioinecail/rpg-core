﻿namespace RPG.Items
{
    using UnityEngine;
    using Newtonsoft.Json;

    [System.Serializable]
    public class ItemDescriptor
    {
        public int ItemId;
        public string Name;
        public string Description;
        public ItemType ItemType;
        public string SpriteId;
        public bool IdentifiedOnStart;
        public bool UsableOnEntities;
        public bool IsCursed;

        public string EquipAction;
        public string UnequipAction;
        public string UseAction;
    }

    public static class ItemUtility
    {
        public static Item[] CreateItems(string json)
        {
            ItemDescriptor[] itemDescriptors = JsonConvert.DeserializeObject<ItemsWrapper>(json).items;

            Item[] items = new Item[itemDescriptors.Length];

            for (int i = 0; i < itemDescriptors.Length; i++)
            {
                items[i] = new Item(itemDescriptors[i]);
            }

            return items;
        }

        public static GameObject GetPrefab(Item target)
        {
            // return an interactable prefab from pool
            // assign a proper sprite to it, that's it
            return null;
        }

        public static string GetSpriteId(Item target)
        {
            return target.SpriteId + 0;
        }

        public static Sprite GetSprite(string spriteId)
        {
            return null;
        }
    }

    [System.Serializable]
    public class ItemsWrapper
    {
        public ItemDescriptor[] items;
    }
}