﻿namespace RPG.Items
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using RPG.Actions;

    public enum ItemType
    {
        POTION,
        SCROLL,
        BOOK,
        FOOD,
        EQUIP
    }

    /// <summary>
    /// Basic implementation of an item
    /// </summary>
    [System.Serializable]
    public class Item
    {
        // info
        public int ItemId { get; protected set; }
        public string Name { get; protected set; }
        public string Description { get; protected set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ItemType ItemType { get; protected set; }
        public string SpriteId { get; protected set; }
        public bool IdentifiedOnStart { get; protected set; }
        public bool UsableOnEntities { get; protected set; }
        public bool IsCursed { get; protected set; }
        public bool IsIdentified { get; protected set; }

        // actions
        public EquipAction Equip { get; protected set; }
        public UnequipAction Unequip { get; protected set; }
        public UseAction Use { get; protected set; }



        public Item (ItemDescriptor descriptor)
        {
            ItemId = descriptor.ItemId;
            Name = descriptor.Name;
            Description = descriptor.Description;
            ItemType = descriptor.ItemType;
            SpriteId = descriptor.SpriteId;
            IdentifiedOnStart = descriptor.IdentifiedOnStart;
            UsableOnEntities = descriptor.UsableOnEntities;
            IsCursed = descriptor.IsCursed;
            IsIdentified = IdentifiedOnStart;

            Equip = null;
            Unequip = null;
            Use = null;
        }

        // methods
        public void Identify()
        {
            IsIdentified = true;
        }

        public void SetCurseState(bool cursed)
        {
            IsCursed = cursed;
        }
    }
}