﻿namespace RPG.Items
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using RPG.Core;
    using Utility;

    /// <summary>
    /// Class used to generate X amount of modifiers based on input
    /// </summary>
    public static class ModifierGenerator
    {
        public const int RANDOM_STAT_MIN = -2;
        public const int RANDOM_STAT_MAX = 4;
        public const int BASE_MODIFIER_PROBABILITY = 50;



        public static List<CharacterStat> GenerateModifiers(Stat[] possibleModifiers)
        {
            List<CharacterStat> modifiers = new List<CharacterStat>();

            for (int i = 0; i < possibleModifiers.Length; i++)
            {
                if (UnityEngine.Random.Range(0, 101) < BASE_MODIFIER_PROBABILITY)
                    continue;

                CharacterStat newModifier = new CharacterStat();

                newModifier.statType = possibleModifiers[i];
                newModifier.value = GenerateStatValue();

                modifiers.Add(newModifier);
            }

            return modifiers;
        }

        private static int GenerateStatValue()
        {
             return GameManager.mainRandom.Next(RANDOM_STAT_MIN, RANDOM_STAT_MAX);
        }
    }
}