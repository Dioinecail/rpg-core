﻿namespace RPG.Abilities
{
    using RPG.Core;
    using RPG.Damage;
    using System;
    using UnityEngine;

    public abstract class AbilityBase : ScriptableObject
    {
        public event Action<object, object> onAbilityBeginEvent;
        public event Action<object, object> onAbilityEvent;
        public event Action<object, object> onAbilityEndEvent;

        [SerializeField] protected IntParameter value;
        [SerializeField] protected IntParameter cost;
        [SerializeField] protected FloatParameter cooldown;
        [SerializeField] protected string abilityName;
        [TextArea(3, 6)]
        [SerializeField] protected string descFormat;
        protected GameEntity cachedSender;
        protected GameEntity cachedTarget;



        public virtual string GetName()
        {
            return abilityName;
        }
        public virtual string GetDesc()
        {
            return descFormat;
        }
        public abstract int GetValue(CharacterStat[] stats);
        public abstract int GetCost(CharacterStat[] stats);
        public abstract float GetCooldown(CharacterStat[] stats);

        public abstract AbilityInstance Cast(Vector3 castPosition, Quaternion castDirecion, GameEntity sender, GameEntity target);

        protected virtual void OnAbilityBegin(object sender, object target)
        {
            onAbilityBeginEvent?.Invoke(sender, target);
        }
        protected virtual void OnAbility(object sender, object target)
        {
            onAbilityEvent?.Invoke(sender, target);
        }
        protected virtual void OnAbilityEnd(object sender, object target)
        {
            onAbilityEndEvent?.Invoke(sender, target);
        }
    }
}