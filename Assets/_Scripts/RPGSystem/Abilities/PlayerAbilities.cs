﻿namespace RPG.Abilities
{
    using RPG.Core;
    using RPG.Input;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class PlayerAbilities
    {
        public event Action<AbilityBase> onSelectedAbilityChanged;
        public event Action<AbilityBase> onAbilityAdded;
        public event Action<AbilityBase, int> onAbilityRemoved;

        public List<AbilityBase> Abilities { get; private set; }
        public AbilityBase this[int index] { get => Abilities[index]; }

        private int selectedAbility;



        public PlayerAbilities(CharacterClass character)
        {
            Abilities = new List<AbilityBase>(character.GetStartingAbilities());
            selectedAbility = 0;

            InputManager.onChangeSpellLeftButtonPressedEvent += OnChangeAbilityLeftButtonPressed;
            InputManager.onChangeSpellRightButtonPressedEvent += OnChangeAbilityRightButtonPressed;
        }

        public bool AddAbility(AbilityBase target)
        {
            if (Abilities.Contains(target))
                return false;
            else
            {
                Abilities.Add(target);
                onAbilityAdded?.Invoke(target);
                return true;
            }
        }

        public bool RemoveAbility(AbilityBase target)
        {
            if (Abilities.Contains(target))
            {
                int index = Abilities.IndexOf(target);
                Abilities.RemoveAt(index);
                onAbilityRemoved(target, index);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void OnChangeAbilityLeftButtonPressed()
        {
            selectedAbility--;

            if (selectedAbility < 0)
                selectedAbility = Abilities.Count - 1;

            onSelectedAbilityChanged?.Invoke(this[selectedAbility]);
        }

        private void OnChangeAbilityRightButtonPressed()
        {
            selectedAbility++;

            if (selectedAbility > Abilities.Count - 1)
                selectedAbility = 0;

            onSelectedAbilityChanged?.Invoke(this[selectedAbility]);
        }
    }
}