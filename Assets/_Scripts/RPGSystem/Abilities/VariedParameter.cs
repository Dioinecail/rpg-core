﻿namespace RPG.Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    /// <summary>
    /// Example:
    /// FLAT: resultDamage = 10 baseDamage (+/-/*) 10 INT
    /// STAT_PERCENT: resultDamage = 10 baseDamage (+/-/*) 10% of INT
    /// STAT_PERCENT_OF_BASEVALUE: resultDamage = 10 baseDamage (+/-/*) (baseDamage * 10% of INT)
    /// </summary>
    public enum ValueType
    {
        FLAT,
        STAT_PERCENT,
        STAT_PERCENT_OF_BASEVALUE
    }

    public enum OperationType
    {
        ADD,
        MULTIPLY,
        SUBTRACT,
        DIVIDE
    }

    [Serializable]
    public class AffectStat
    {
        [SerializeField] private ValueType valueType;
        [SerializeField] private OperationType affectType;
        [SerializeField] private Stat statType;
        [SerializeField] private int value;

        public ValueType ValueType { get => valueType; }
        public OperationType AffectionType { get => affectType; }
        public Stat StatType { get => statType; }
        public int Value { get => value; }
    }

    [Serializable]
    public class ParameterModifier
    {
        [SerializeField] private AffectStat[] value;

        public AffectStat this[int index] { get => value[index]; }
        public AffectStat[] Value { get => value; }



        public bool Contains(Stat statType, out int index)
        {
            index = -1;

            for (int i = 0; i < value.Length; i++)
            {
                if (value[i].StatType == statType)
                {
                    index = i;
                    return true;
                }
            }

            return false;
        }

        public void ApplyModifier(int baseValue, ref int targetValue, CharacterStat targetStat)
        {
            int targetValue2 = 0;
            AffectStat affectionStat = null;

            if (Contains(targetStat.statType, out int affectionIndex))
                affectionStat = this[affectionIndex];
            else
                return;

            switch (affectionStat.ValueType)
            {
                case ValueType.FLAT:
                    targetValue2 = targetStat.value;
                    break;
                case ValueType.STAT_PERCENT:
                    targetValue2 = (int)(IntToFloatPercent(affectionStat.Value) * targetStat.value);
                    break;
                case ValueType.STAT_PERCENT_OF_BASEVALUE:
                    targetValue2 = (int)(IntToFloatPercent(affectionStat.Value) * targetStat.value * baseValue);
                    break;
            }

            switch (affectionStat.AffectionType)
            {
                case OperationType.ADD:
                    targetValue += targetValue2;
                    break;
                case OperationType.MULTIPLY:
                    targetValue *= targetValue2;
                    break;
                case OperationType.SUBTRACT:
                    targetValue -= targetValue2;
                    break;
                case OperationType.DIVIDE:
                    targetValue /= targetValue2;
                    break;
            }
        }

        public void ApplyModifier(float baseValue, ref float targetValue, CharacterStat targetStat)
        {
            float value2 = 0;
            AffectStat affectionStat = null;

            if (Contains(targetStat.statType, out int affectionIndex))
                affectionStat = this[affectionIndex];
            else
                return;

            switch (affectionStat.ValueType)
            {
                case ValueType.FLAT:
                    value2 = targetStat.value;
                    break;
                case ValueType.STAT_PERCENT:
                    value2 = (IntToFloatPercent(affectionStat.Value) * targetStat.value);
                    break;
                case ValueType.STAT_PERCENT_OF_BASEVALUE:
                    value2 = (IntToFloatPercent(affectionStat.Value) * targetStat.value * baseValue);
                    break;
            }

            switch (affectionStat.AffectionType)
            {
                case OperationType.ADD:
                    targetValue += value2;
                    break;
                case OperationType.MULTIPLY:
                    targetValue *= value2;
                    break;
                case OperationType.SUBTRACT:
                    targetValue -= value2;
                    break;
                case OperationType.DIVIDE:
                    targetValue /= value2;
                    break;
            }
        }

        public static float IntToFloatPercent(int input)
        {
            return (float)input / 100;
        }
    }

    [Serializable]
    public abstract class VariedParameter<T>
    {
        [SerializeField] protected T baseValue;
        [SerializeField] protected T minValue;
        [SerializeField] protected ParameterModifier modifier;



        public abstract T GetValue(CharacterStat[] stats);
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();

            result.Append(baseValue + " ");

            for (int i = 0; i < modifier.Value.Length; i++)
            {
                switch (modifier[i].AffectionType)
                {
                    case OperationType.ADD:
                        result.Append("+ ");
                        break;
                    case OperationType.MULTIPLY:
                        result.Append("* ");
                        break;
                    case OperationType.SUBTRACT:
                        result.Append("- ");
                        break;
                    case OperationType.DIVIDE:
                        result.Append("/ ");
                        break;
                }

                switch (modifier[i].ValueType)
                {
                    case ValueType.FLAT:
                        result.Append(modifier[i].StatType);
                        break;
                    case ValueType.STAT_PERCENT:
                        result.Append(modifier[i].Value + "%" + modifier[i].StatType);
                        break;
                    case ValueType.STAT_PERCENT_OF_BASEVALUE:
                        result.Append("(" + baseValue + " * " + modifier[i].Value + "%" + modifier[i].StatType + ")");
                        break;
                }

                result.Append(" ");
            }

            return result.ToString();
        }
    }

    [Serializable]
    public class FloatParameter : VariedParameter<float>
    {
        public override float GetValue(CharacterStat[] stats)
        {
            float result = baseValue;

            for (int i = 0; i < stats.Length; i++)
            {
                modifier.ApplyModifier(baseValue, ref result, stats[i]);
            }

            return Mathf.Clamp(result, minValue, float.MaxValue);
        }
    }

    [Serializable]
    public class IntParameter : VariedParameter<int>
    {
        public override int GetValue(CharacterStat[] stats)
        {
            int result = baseValue;

            for (int i = 0; i < stats.Length; i++)
            {
                modifier.ApplyModifier(baseValue, ref result, stats[i]);
            }

            return Mathf.Clamp(result, minValue, int.MaxValue);
        }
    }
}