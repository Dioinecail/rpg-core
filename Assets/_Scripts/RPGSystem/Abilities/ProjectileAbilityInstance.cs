﻿namespace RPG.Abilities
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using RPG.Damage;
    using UnityEngine;

    public class ProjectileAbilityInstance : AbilityInstance
    {
        public event Action onAbilityBegin;
        public event Action onAbilityUpdated;
        public event Action onAbilityEnd;

        public GameEntity sender { get; private set; }
        public GameEntity target { get; private set; }

        private ProjectileAbility cachedAbility;
        private Transform[] cachedProjectiles;
        private Collider[] projectileCollisionBuffer = new Collider[16];



        public void Init(AbilityBase ability, Vector3 castPosition, Quaternion castDirection, GameEntity sender, GameEntity target)
        {
            this.sender = sender;
            this.target = target;

            cachedAbility = ability as ProjectileAbility;

            int totalProjectileCount = cachedAbility.ProjectilesCount.GetValue(sender.GetStats());

            cachedProjectiles = new Transform[totalProjectileCount];

            for (int i = 0; i < totalProjectileCount; i++)
            {
                GameObject newProjectile = GameObject.Instantiate(cachedAbility.ProjectilePrefab, castPosition, castDirection);
                cachedProjectiles[i] = newProjectile.transform;
            }

            onAbilityBegin?.Invoke();
        }

        public void Update()
        {
            UpdateProjectiles();
        }

        public void End()
        {
            onAbilityEnd?.Invoke();
        }

        private void UpdateProjectiles()
        {
            Vector3 targetPosition = Vector3.forward;

            if (target != null)
            {
                targetPosition = target.transform.position;
            }

            for (int i = 0; i < cachedProjectiles.Length; i++)
            {
                if (cachedProjectiles[i] != null)
                {
                    Vector3 direction = targetPosition - cachedProjectiles[i].position;
                    Quaternion targetRotation = Quaternion.LookRotation(direction, Vector3.back);

                    cachedProjectiles[i].rotation = Quaternion.RotateTowards(cachedProjectiles[i].rotation, targetRotation, cachedAbility.ProjectileTurnSpeed.GetValue(sender.GetStats()) * Time.deltaTime);
                    cachedProjectiles[i].position += (cachedProjectiles[i].forward * cachedAbility.ProjectileMoveSpeed.GetValue(sender.GetStats()) * Time.deltaTime);

                    if (CheckProjectileTargetOverlap(cachedProjectiles[i].position))
                    {
                        GameObject.Destroy(cachedProjectiles[i].gameObject);
                        cachedProjectiles[i] = null;
                    }
                }
            }

            onAbilityUpdated?.Invoke();
        }

        private void ClearCollisionBuffer()
        {
            for (int i = 0; i < projectileCollisionBuffer.Length; i++)
            {
                projectileCollisionBuffer[i] = null;
            }
        }

        private bool CheckProjectileTargetOverlap(Vector3 position)
        {
            ClearCollisionBuffer();

            Physics.OverlapSphereNonAlloc(position, cachedAbility.ProjectileRadius.GetValue(sender.GetStats()), projectileCollisionBuffer, cachedAbility.ProjectileCollisionMask, QueryTriggerInteraction.Collide);

            for (int i = 0; i < projectileCollisionBuffer.Length; i++)
            {
                if (projectileCollisionBuffer[i] != null)
                {
                    GameEntity entity = projectileCollisionBuffer[i].GetComponent<GameEntity>();

                    if (entity != null)
                    {
                        entity.DealDamage(cachedAbility.GetValue(sender.GetStats()));
                    }
                }
            }

            return projectileCollisionBuffer[0] != null;
        }
    }
}