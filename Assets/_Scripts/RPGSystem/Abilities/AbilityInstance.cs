﻿namespace RPG.Abilities
{
    using RPG.Damage;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public interface AbilityInstance
    {
        event Action onAbilityBegin;
        event Action onAbilityUpdated;
        event Action onAbilityEnd;

        GameEntity sender { get; }
        GameEntity target { get; }

        void Init(AbilityBase ability, Vector3 castPosition, Quaternion castDirection, GameEntity sender, GameEntity target);
        void Update();
        void End();
    }
}