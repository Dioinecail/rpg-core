﻿namespace RPG.Abilities
{
    using RPG.Core;
    using RPG.Damage;
    using RPG.Enemy;
    using System.Text;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Abilities/Projectile", fileName = "ProjectileAbility_")]
    public class ProjectileAbility : AbilityBase
    {
        [SerializeField] protected GameObject projectilePrefab;
        [SerializeField] protected bool isHoming;
        [SerializeField] protected IntParameter projectilesCount;
        [SerializeField] protected FloatParameter projectileMoveSpeed;
        [SerializeField] protected FloatParameter projectileTurnSpeed;
        [SerializeField] protected FloatParameter projectileRadius;
        [SerializeField] protected LayerMask projectileCollisionMask;
        
        public GameObject ProjectilePrefab { get => projectilePrefab; }
        public IntParameter ProjectilesCount{ get => projectilesCount; }
        public FloatParameter ProjectileMoveSpeed{ get => projectileMoveSpeed; }
        public FloatParameter ProjectileTurnSpeed{ get => projectileTurnSpeed; }
        public FloatParameter ProjectileRadius{ get => projectileRadius; }
        public LayerMask ProjectileCollisionMask{ get => projectileCollisionMask; }



        public override float GetCooldown(CharacterStat[] stats)
        {
            return cooldown.GetValue(stats);
        }

        public override int GetCost(CharacterStat[] stats)
        {
            return cost.GetValue(stats);
        }

        public override int GetValue(CharacterStat[] stats)
        {
            return value.GetValue(stats);
        }

        public override string GetDesc()
        {
            StringBuilder descString = new StringBuilder();

            descString.AppendLine("Damage: " + value.ToString());
            descString.AppendLine("Cost: " + cost.ToString());
            descString.AppendLine("Cooldown: " + cooldown.ToString());
            descString.AppendLine("Projectiles: " + projectilesCount.ToString());

            return descString.ToString();
        }

        public override AbilityInstance Cast(Vector3 castPosition, Quaternion castDirection, GameEntity sender, GameEntity target)
        {
            int cost = GetCost(sender.GetStats());

            if (sender.Mana.CurrentValue < cost)
                return null;

            AbilityInstance newInstance = new ProjectileAbilityInstance();
            newInstance.Init(this, castPosition, castDirection, sender, target);

            return newInstance;
        }
    }
}