﻿namespace RPG.Actions
{
    using RPG.Items;
    using RPG.Core;
    using RPG.Equipment;

    public class EquipAction : Action
    {
        public Item item;
        public EquipCategory targetCategory;
        public Player target;



        public override ActionResult Perform()
        {
            // equip an item onto target player
            bool equipResult = target.Equipment.EquipItem(item, targetCategory);

            if (equipResult)
                return new ActionResult { result = Result.SUCCESS };
            else
                return new ActionResult { result = Result.FAILURE };
        }
    }

    public class UnequipAction : Action
    {
        public Item item;
        public EquipCategory targetCategory;
        public Player target;



        public override ActionResult Perform()
        {
            // equip an item onto target player
            bool equipResult = target.Equipment.UnequipItem(targetCategory);

            if (equipResult)
                return new ActionResult { result = Result.SUCCESS };
            else
                return new ActionResult { result = Result.FAILURE };
        }
    }
}