﻿namespace RPG.Actions
{
    using RPG.Core;
    using RPG.Damage;
    using RPG.Items;
    using System.Collections.Generic;

    public enum Result
    {
        SUCCESS,
        FAILURE
    }

    public struct ActionResult
    {
        public Result result;
    }

    public abstract class Action
    {
        public abstract ActionResult Perform();
    }

    public abstract class UseAction : Action
    {
        public Item source;
        public object target;
        public List<ItemType> usableOnTypes;



        public override ActionResult Perform()
        {
            if (target is Item)
            {
                Item targetItem = target as Item;
                ItemType targetType = targetItem.ItemType;

                if (usableOnTypes.Contains(targetType))
                    return DoPerform();
            }
            else if (target is GameEntity)
                if (source.UsableOnEntities)
                    return DoPerform();

            return new ActionResult { result = Result.FAILURE };
        }

        public abstract ActionResult DoPerform();
    }

    public class HealUseAction : UseAction
    {
        public int healAmount;



        public override ActionResult DoPerform()
        {
            GameEntity entity = target as GameEntity;

            entity.Heal(healAmount);

            return new ActionResult { result = Result.SUCCESS };
        }
    }
}