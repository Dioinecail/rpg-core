﻿namespace RPG.UI
{
    using RPG.Core;
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class MenuBase : MonoBehaviour
    {
        public string headerText;

        public UnityEvent onMenuOpenedEvent;
        public UnityEvent onMenuClosedEvent;

        protected bool IsActive { get; set; }
        protected Player targetPlayer;



        public virtual void InitMenu(Player player)
        {
            targetPlayer = player;
        }

        public virtual void OpenMenu()
        {
            IsActive = true;
            onMenuOpenedEvent?.Invoke();
        }

        public virtual void CloseMenu()
        {
            IsActive = false;
            onMenuClosedEvent?.Invoke();
        }
    }
}