﻿namespace RPG.UI
{
    using System.Collections;
    using System.Collections.Generic;
    using RPG.Abilities;
    using RPG.Core;
    using RPG.Input;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class PlayerAbilitiesMenu : MenuBase
    {
        public RectTransform abilitiesRoot;
        public ScrollRect abilitiesScrollRect;
        public GameObject abilityButtonPrefab;
        public TMP_Text abilityName;
        public TMP_Text abilityDesc;

        private List<GameObject> abilitiesList;
        private PlayerAbilities targetAbilities;
        private int selectedAbility;



        public override void InitMenu(Player player)
        {
            targetAbilities = player.Abilities;
            selectedAbility = 0;
            abilitiesList = new List<GameObject>();

            for (int i = 0; i < targetAbilities.Abilities.Count; i++)
            {
                GameObject newAbilityButton = Instantiate(abilityButtonPrefab, abilitiesRoot);
                TMP_Text abilityText = newAbilityButton.GetComponent<TMP_Text>();
                abilityText.text = targetAbilities.Abilities[i].GetName() + "_" + i.ToString();
                abilitiesList.Add(newAbilityButton);
            }

            targetAbilities.onAbilityAdded += OnAbilityAdded;
        }
        
        public override void OpenMenu()
        {
            base.OpenMenu();

            selectedAbility = 0;
            OnAbilitySelected(selectedAbility);
        }

        private void OnAbilityAdded(AbilityBase ability)
        {
            GameObject newAbilityButton = Instantiate(abilityButtonPrefab, abilitiesRoot);
            TMP_Text abilityText = newAbilityButton.GetComponent<TMP_Text>();
            abilityText.text = ability.GetName();
            abilitiesList.Add(newAbilityButton);
        }

        private void OnAbilityRemoved(AbilityBase ability, int index)
        {
            GameObject removedAbility = abilitiesList[index];

            abilitiesList.Remove(removedAbility);

            Destroy(removedAbility);
        }

        #region HANDLING INFO DISPLAY

        private void OnAbilitySelected(int index)
        {
            var stuff = abilitiesList[index].GetComponent<TMP_Text>();
            stuff.color = Color.cyan;

            float oneMinusCurrent = 1 - ((float)(index) / (targetAbilities.Abilities.Count - 1));
            abilitiesScrollRect.verticalNormalizedPosition = oneMinusCurrent;

            DisplayAbilityInfo(targetAbilities[index]);
        }

        private void DisplayAbilityInfo(AbilityBase target)
        {
            abilityName.text = target.GetName();
            abilityDesc.text = target.GetDesc();
        }

        #endregion

        #region HANDLING INPUTS

        private void OnDownButtonPressed()
        {
            if (!IsActive)
                return;

            var stuff = abilitiesList[selectedAbility].GetComponent<TMP_Text>();
            stuff.color = Color.white;

            selectedAbility++;

            if (selectedAbility > targetAbilities.Abilities.Count - 1)
                selectedAbility = 0;

            OnAbilitySelected(selectedAbility);
        }

        private void OnUpButtonPressed()
        {
            if (!IsActive)
                return;

            var stuff = abilitiesList[selectedAbility].GetComponent<TMP_Text>();
            stuff.color = Color.white;

            selectedAbility--;

            if (selectedAbility < 0)
                selectedAbility = targetAbilities.Abilities.Count - 1;

            OnAbilitySelected(selectedAbility);
        }

        #endregion


        #region EVENTS SUBS

        private void OnEnable()
        {
            InputManager.onMenuButtonUpPressedEvent += OnUpButtonPressed;
            InputManager.onMenuButtonDownPressedEvent += OnDownButtonPressed;
        }

        private void OnDisable()
        {
            InputManager.onMenuButtonUpPressedEvent -= OnUpButtonPressed;
            InputManager.onMenuButtonDownPressedEvent -= OnDownButtonPressed;
        }

        #endregion
    }
}