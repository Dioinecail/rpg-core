﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Core;
using RPG.Equipment;
using TMPro;
using System;
using System.Text;
using UnityEngine.UI;
using UnityEngine.Events;
using RPG.Inventory;

public class DEBUG_Player : MonoBehaviour
{
    public Action<CharacterClass> onCharacterInited;

    public TMP_Text DEBUG_stats;
    public TMP_Text DEBUG_equipmentStats;

    public int DEBUG_modifiersAmount = 3;
    public int DEBUG_index;



    public void DEBUG_InitCharacter(CharacterClass character)
    {
        Player p = GameManager.Instance.CreatePlayer(DEBUG_index, character);

        DisplayPlayerStats();

        onCharacterInited?.Invoke(character);
    }

    private void DisplayPlayerStats()
    {
        var allStats = Enum.GetNames(typeof(Stat));
        StringBuilder statsString = new StringBuilder();

        for (int i = 0; i < allStats.Length; i++)
        {
            Stat selected;
            if (Enum.TryParse(allStats[i], out selected))
            {
                int statValue = GameManager.Instance.GetPlayer(DEBUG_index).GetTotalStat(selected);

                statsString.AppendFormat("[{0}]:[{1}]\n", allStats[i], statValue);
            }
        }

        DEBUG_stats.text = statsString.ToString();
    }

}