﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Items;

public class DEBUG_ItemsTest : MonoBehaviour
{
    public TextAsset itemsJson;

    [ContextMenu("test parsing")]
    public void TestJsonParsing()
    {
        Item[] items = ItemUtility.CreateItems(itemsJson.text);

        for (int i = 0; i < items.Length; i++)
        {
            Debug.Log($"Item:{items[i].Name}");
        }
    }
}
