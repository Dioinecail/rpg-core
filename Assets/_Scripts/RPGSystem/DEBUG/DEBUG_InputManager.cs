﻿namespace RPG.DEBUG
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using RPG.Input;

    public class DEBUG_InputManager : MonoBehaviour
    {
        public InputManager inputManager;

        public void SetUIInput()
        {
            inputManager.ChangeInputState(InputState.Menu);
        }

        public void SetGameplayInput()
        {
            inputManager.ChangeInputState(InputState.Gameplay);
        }

        private void InputManager_onMenuButtonCancelPressedEvent()
        {
            Debug.Log("InputManager_onMenuButtonCancelPressedEvent");
        }

        private void InputManager_onMenuButtonConfirmPressedEvent()
        {
            Debug.Log("InputManager_onMenuButtonConfirmPressedEvent");
        }

        private void InputManager_onMenuButtonPageRightPressedEvent()
        {
            Debug.Log("InputManager_onMenuButtonPageRightPressedEvent");
        }

        private void InputManager_onMenuButtonPageLeftPressedEvent()
        {
            Debug.Log("InputManager_onMenuButtonPageLeftPressedEvent");
        }

        private void InputManager_onMenuButtonLeftPressedEvent()
        {
            Debug.Log("InputManager_onMenuButtonLeftPressedEvent");
        }

        private void InputManager_onMenuButtonRightPressedEvent()
        {
            Debug.Log("InputManager_onMenuButtonRightPressedEvent");
        }

        private void InputManager_onMenuButtonDownPressedEvent()
        {
            Debug.Log("InputManager_onMenuButtonDownPressedEvent");
        }

        private void InputManager_onMenuButtonUpPressedEvent()
        {
            Debug.Log("InputManager_onMenuButtonUpPressedEvent");
        }
        
        private void InputManager_onChangeSpellRightButtonPressedEvent()
        {
            Debug.Log("InputManager_onChangeSpellRightButtonPressedEvent");
        }

        private void InputManager_onChangeSpellLeftButtonPressedEvent()
        {
            Debug.Log("InputManager_onChangeSpellLeftButtonPressedEvent");
        }

        private void InputManager_onSecondaryButtonPressedEvent()
        {
            Debug.Log("InputManager_onSecondaryButtonPressedEvent");
        }

        private void InputManager_onInteractButtonPressedEvent()
        {
            Debug.Log("InputManager_onInteractButtonPressedEvent");
        }

        private void InputManager_onRangedButtonUnpressedEvent()
        {
            Debug.Log("InputManager_onRangedButtonUnpressedEvent");
        }

        private void InputManager_onRangedButtonPressedEvent()
        {
            Debug.Log("InputManager_onRangedButtonPressedEvent");
        }

        private void InputManager_onMagicButtonUnpressedEvent()
        {
            Debug.Log("InputManager_onMagicButtonUnpressedEvent");
        }

        private void InputManager_onMagicButtonPressedEvent()
        {
            Debug.Log("InputManager_onMagicButtonPressedEvent");
        }

        private void InputManager_onMeleeAttackButtonUnpressedEvent()
        {
            Debug.Log("InputManager_onMeleeAttackButtonUnpressedEvent");
        }

        private void InputManager_onMeleeAttackButtonPressedEvent()
        {
            Debug.Log("InputManager_onMeleeAttackButtonPressedEvent");
        }

        private void InputManager_onJumpButtonUnpressedEvent()
        {
            Debug.Log("InputManager_onJumpButtonUnpressedEvent");
        }

        private void InputManager_onJumpButtonPressedEvent()
        {
            Debug.Log("InputManager_onJumpButtonPressedEvent");
        }

        private void InputManager_onVerticalInputsEvent(float obj)
        {
            if (Mathf.Abs(obj) > 0.01f) 
                Debug.Log($"InputManager_onVerticalInputsEvent: {obj.ToString("0.0")}");
        }

        private void InputManager_onHorizontalInputsEvent(float obj)
        {
            if (Mathf.Abs(obj) > 0.01f)
                Debug.Log($"InputManager_onHorizontalInputsEvent: {obj.ToString("0.0")}");
        }

        private void OnEnable()
        {
            InputManager.onMenuButtonUpPressedEvent += InputManager_onMenuButtonUpPressedEvent;
            InputManager.onMenuButtonDownPressedEvent += InputManager_onMenuButtonDownPressedEvent;
            InputManager.onMenuButtonRightPressedEvent += InputManager_onMenuButtonRightPressedEvent;
            InputManager.onMenuButtonLeftPressedEvent += InputManager_onMenuButtonLeftPressedEvent;
            InputManager.onMenuButtonPageLeftPressedEvent += InputManager_onMenuButtonPageLeftPressedEvent;
            InputManager.onMenuButtonPageRightPressedEvent += InputManager_onMenuButtonPageRightPressedEvent;
            InputManager.onMenuButtonConfirmPressedEvent += InputManager_onMenuButtonConfirmPressedEvent;
            InputManager.onMenuButtonCancelPressedEvent += InputManager_onMenuButtonCancelPressedEvent;

            InputManager.onHorizontalInputsEvent += InputManager_onHorizontalInputsEvent;
            InputManager.onVerticalInputsEvent += InputManager_onVerticalInputsEvent;
            InputManager.onJumpButtonPressedEvent += InputManager_onJumpButtonPressedEvent;
            InputManager.onJumpButtonUnpressedEvent += InputManager_onJumpButtonUnpressedEvent;
            InputManager.onMainAttackButtonPressedEvent += InputManager_onMeleeAttackButtonPressedEvent;
            InputManager.onMainAttackButtonUnpressedEvent += InputManager_onMeleeAttackButtonUnpressedEvent;
            InputManager.onMagicButtonPressedEvent += InputManager_onMagicButtonPressedEvent;
            InputManager.onMagicButtonUnpressedEvent += InputManager_onMagicButtonUnpressedEvent;
            InputManager.onSkillAttackButtonPressedEvent += InputManager_onRangedButtonPressedEvent;
            InputManager.onSkillAttackButtonUnpressedEvent += InputManager_onRangedButtonUnpressedEvent;
            InputManager.onInteractButtonPressedEvent += InputManager_onInteractButtonPressedEvent;
            InputManager.onSecondaryButtonPressedEvent += InputManager_onSecondaryButtonPressedEvent;
            InputManager.onChangeSpellLeftButtonPressedEvent += InputManager_onChangeSpellLeftButtonPressedEvent;
            InputManager.onChangeSpellRightButtonPressedEvent += InputManager_onChangeSpellRightButtonPressedEvent;
        }

        private void OnDisable()
        {
            InputManager.onMenuButtonUpPressedEvent -= InputManager_onMenuButtonUpPressedEvent;
            InputManager.onMenuButtonDownPressedEvent -= InputManager_onMenuButtonDownPressedEvent;
            InputManager.onMenuButtonRightPressedEvent -= InputManager_onMenuButtonRightPressedEvent;
            InputManager.onMenuButtonLeftPressedEvent -= InputManager_onMenuButtonLeftPressedEvent;
            InputManager.onMenuButtonPageLeftPressedEvent -= InputManager_onMenuButtonPageLeftPressedEvent;
            InputManager.onMenuButtonPageRightPressedEvent -= InputManager_onMenuButtonPageRightPressedEvent;
            InputManager.onMenuButtonConfirmPressedEvent -= InputManager_onMenuButtonConfirmPressedEvent;
            InputManager.onMenuButtonCancelPressedEvent -= InputManager_onMenuButtonCancelPressedEvent;

            InputManager.onHorizontalInputsEvent -= InputManager_onHorizontalInputsEvent;
            InputManager.onVerticalInputsEvent -= InputManager_onVerticalInputsEvent;
            InputManager.onJumpButtonPressedEvent -= InputManager_onJumpButtonPressedEvent;
            InputManager.onJumpButtonUnpressedEvent -= InputManager_onJumpButtonUnpressedEvent;
            InputManager.onMainAttackButtonPressedEvent -= InputManager_onMeleeAttackButtonPressedEvent;
            InputManager.onMainAttackButtonUnpressedEvent -= InputManager_onMeleeAttackButtonUnpressedEvent;
            InputManager.onMagicButtonPressedEvent -= InputManager_onMagicButtonPressedEvent;
            InputManager.onMagicButtonUnpressedEvent -= InputManager_onMagicButtonUnpressedEvent;
            InputManager.onSkillAttackButtonPressedEvent -= InputManager_onRangedButtonPressedEvent;
            InputManager.onSkillAttackButtonUnpressedEvent -= InputManager_onRangedButtonUnpressedEvent;
            InputManager.onInteractButtonPressedEvent -= InputManager_onInteractButtonPressedEvent;
            InputManager.onSecondaryButtonPressedEvent -= InputManager_onSecondaryButtonPressedEvent;
            InputManager.onChangeSpellLeftButtonPressedEvent -= InputManager_onChangeSpellLeftButtonPressedEvent;
            InputManager.onChangeSpellRightButtonPressedEvent -= InputManager_onChangeSpellRightButtonPressedEvent;
        }
    }
}