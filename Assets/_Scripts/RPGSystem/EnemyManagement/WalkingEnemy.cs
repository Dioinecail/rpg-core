﻿namespace RPG.Enemy
{
    using UnityEngine;

    public class WalkingEnemy : EnemyBase
    {
        [SerializeField] protected Vector2 checkOffset;
        [SerializeField] protected LayerMask checkMask;
        [SerializeField] protected float movementSpeed;
        [SerializeField] protected float checkDistance;

        protected int currentDirection = 1;
        protected RaycastHit[] checkBuffer = new RaycastHit[1];



        public override void Init()
        {
            base.Init();

            if (Random.Range(0, 2) > 0)
                currentDirection = -currentDirection;
        }

        protected virtual void Update()
        {
            if (!IsStaggered)
                Move();
        }

        protected virtual void Move()
        {
            if (CheckFrontIsInvalid())
                currentDirection = -currentDirection;

            rBody.velocity = currentDirection * Vector3.right * movementSpeed;

            if(Mathf.Abs(rBody.velocity.x) > 0.01f)
            {
                Vector3 direction = new Vector3(Mathf.Sign(rBody.velocity.x), 0, 0);
                Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);

                targetModel.rotation = rotation;
            }
        }

        private bool CheckFrontIsInvalid()
        {
            bool isInvalid = true;

            ClearBuffer();

            Vector3 offset = new Vector3(checkOffset.x * currentDirection, checkOffset.y);

            Physics.RaycastNonAlloc(transform.position + offset, Vector3.down, checkBuffer, checkDistance, checkMask, QueryTriggerInteraction.Ignore);

            for (int i = 0; i < checkBuffer.Length; i++)
            {
                if (checkBuffer[i].collider != null)
                    isInvalid = false;
            }

            ClearBuffer();

            Physics.RaycastNonAlloc(transform.position + offset, Vector3.right * currentDirection, checkBuffer, checkDistance, checkMask, QueryTriggerInteraction.Ignore);

            for (int i = 0; i < checkBuffer.Length; i++)
            {
                if (checkBuffer[i].collider != null)
                    isInvalid = true;
            }

            return isInvalid;
        }

        private void ClearBuffer()
        {
            for (int i = 0; i < checkBuffer.Length; i++)
            {
                checkBuffer[i] = new RaycastHit();
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.position + (Vector3)checkOffset, Vector3.down * checkDistance);
        }
    }
}