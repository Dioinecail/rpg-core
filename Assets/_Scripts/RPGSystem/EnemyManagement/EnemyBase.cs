﻿namespace RPG.Enemy
{
    using UnityEngine;
    using RPG.Damage;
    using System.Collections;
    using UnityEngine.Events;
    using RPG.Core;

    public class EnemyBase : GameEntity
    {
        [SerializeField] protected Transform targetModel;
        [SerializeField] protected IntParameter damage;

        protected bool IsStaggered
        {
            get
            {
                return coroutine_Stagger != null;
            }
        }
        protected Coroutine coroutine_Stagger;
        protected Rigidbody rBody;

        public UnityEvent onGetHitEvent;



        public override void Init()
        {
            base.Init();

            rBody = GetComponent<Rigidbody>();

            health.onParameterChangedEvent += OnHealthChanged;
        }

        private void OnHealthChanged(ParameterChangedEventArgs args)
        {
            if (args.isDamage)
                onGetHitEvent?.Invoke();
        }

        public override void DealDamage(int amount)
        {
            base.DealDamage(amount);

            Stagger(1);
        }

        protected override void OnDestroyed()
        {
            Destroy(gameObject);
        }

        public int GetDamage()
        {
            return damage.GetValue(GetStats());
        }

        public void Stagger(float duration)
        {
            if (coroutine_Stagger != null)
                StopCoroutine(coroutine_Stagger);

            coroutine_Stagger = StartCoroutine(StaggerCoroutine(duration));
        }

        protected IEnumerator StaggerCoroutine(float duration)
        {
            yield return new WaitForSeconds(duration);

            coroutine_Stagger = null;
        }
    }
}