﻿namespace RPG.Damage
{
    using RPG.Core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    
    public abstract class GameEntity : MonoBehaviour, StatContainer
    {
        public event Action<GameEntity> onEntityDied;

        [SerializeField] protected CharacterStat[] stats;

        protected ObservableParameter health;
        protected ObservableParameter mana;
        protected Dictionary<Stat, int> statsDictionry;
        protected Dictionary<Stat, List<int>> modifiers;

        private bool isDead = false;

        public ObservableParameter Health { get => health; }
        public ObservableParameter Mana { get => mana; }



        public virtual void Init()
        {
            statsDictionry = new Dictionary<Stat, int>();

            for (int i = 0; i < stats.Length; i++)
            {
                statsDictionry.Add(stats[i].statType, stats[i].value);
            }

            if(statsDictionry.ContainsKey(Stat.VIT))
                health = new ObservableParameter(statsDictionry[Stat.VIT]);
            else
                health = new ObservableParameter(1);

            if(statsDictionry.ContainsKey(Stat.INT))
                mana = new ObservableParameter(statsDictionry[Stat.INT]);
            else
                mana = new ObservableParameter(1);

            health.onParameterChangedEvent += OnHealthChangedEvent;
            mana.onParameterChangedEvent += OnManaChangedEvent;
        }

        public int GetStat(Stat sType)
        {
            if (statsDictionry.ContainsKey(sType))
                return statsDictionry[sType];
            else
                return 0;
        }

        public virtual void DealDamage(int amount)
        {
            health.DecreaseCurrent(amount);
        }

        public virtual void Heal(int amount)
        {
            health.IncreaseCurrent(amount);
        }

        protected virtual void OnHealthChangedEvent(ParameterChangedEventArgs args)
        {
            if(args.newValue == 0 && !isDead)
            {
                isDead = true;
                OnDestroyed();
            }
        }

        protected virtual void OnManaChangedEvent(ParameterChangedEventArgs args)
        {
            // notify anyone that mana has changed if they need it!
        }

        protected virtual void OnDestroyed()
        {
            onEntityDied?.Invoke(this);
        }

        public virtual CharacterStat[] GetStats()
        {
            return stats;
        }
    }
}