﻿namespace RPG.Damage
{
    using RPG.Core;
    using RPG.Enemy;

    public static class DamageSystem
    {
        public static int CalculatePhysicalMeleeDamage(Player sender, GameEntity target)
        {
            int targetDef = target.GetStat(Stat.DEF);
            int senderAtk = sender.GetDamage(Equipment.EquipCategory.WEAPON, Stat.STR);

            return senderAtk - targetDef;
        }

        public static int CalculatePhysicalRangedDamage(Player sender, GameEntity target)
        {
            int targetDef = target.GetStat(Stat.DEF);
            int senderAtk = sender.GetDamage(Equipment.EquipCategory.WEAPON, Stat.DEX);

            return senderAtk - targetDef;
        }

        public static int CalculateMagicalDamage(Player sender, GameEntity target)
        {
            int targetDef = target.GetStat(Stat.MDF);
            int senderAtk = sender.GetDamage(Equipment.EquipCategory.WEAPON, Stat.INT);

            return senderAtk - targetDef;
        }

        public static int CalculatePhysicalDamage(EnemyBase sender, Player target)
        {
            int targetDef = target.GetTotalStat(Stat.DEF);
            int senderAtk = sender.GetDamage();

            return senderAtk - targetDef;
        }

        public static int CalculateMagicalDamage(EnemyBase sender, Player target)
        {
            int targetDef = target.GetTotalStat(Stat.MDF);
            int senderAtk = sender.GetDamage();

            return senderAtk - targetDef;
        }
    }
}