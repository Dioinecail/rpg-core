﻿namespace RPG.Damage
{
    using System;
    using UnityEngine;

    public class ParameterChangedEventArgs
    {
        public bool isDamage;
        public int max, oldValue, newValue;
    }

    public class ParameterMaxValueChangedEventArgs
    {
        public int oldValue, newValue;
    }

    public class ObservableParameter
    {
        public event Action<ParameterChangedEventArgs> onParameterChangedEvent;
        public event Action<ParameterMaxValueChangedEventArgs> onParameterMaxValueChangedEvent;

        [SerializeField] private int valuePerStatMultiplier = 10;

        private int maximumValue;
        private int currentValue;

        public int MaxValue { get => maximumValue; }
        public int CurrentValue { get => currentValue; }



        public ObservableParameter (int targetStat)
        {
            maximumValue = valuePerStatMultiplier * targetStat;
            currentValue = maximumValue;
        }

        public void IncreaseCurrent(int amount)
        {
            ChangeCurrentValue(amount);
        }

        public void DecreaseCurrent(int amount)
        {
            ChangeCurrentValue(-amount);
        }

        public void IncreaseMaxValue(int amount)
        {
            ChangeMaxValue(amount);
        }

        public void DecreaseMaxValue(int amount)
        {
            ChangeMaxValue(-amount);
        }

        private void ChangeCurrentValue(int amount)
        {
            ParameterChangedEventArgs args = new ParameterChangedEventArgs();
            args.isDamage = amount < 0;
            args.max = maximumValue;
            args.oldValue = currentValue;

            currentValue = Mathf.Clamp(currentValue + amount, 0, maximumValue);
            args.newValue = currentValue;

            onParameterChangedEvent?.Invoke(args);
        }

        private void ChangeMaxValue(int amount)
        {
            ParameterMaxValueChangedEventArgs args = new ParameterMaxValueChangedEventArgs()
            {
                oldValue = maximumValue,
                newValue = maximumValue + amount
            };

            maximumValue = Mathf.Clamp(maximumValue + amount, 0, int.MaxValue);

            onParameterMaxValueChangedEvent?.Invoke(args);
        }
    }
}