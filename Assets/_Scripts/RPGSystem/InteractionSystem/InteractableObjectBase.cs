﻿namespace RPG.InteractionSystem
{
    using RPG.Core;
    using UnityEngine;

    [RequireComponent(typeof(SphereCollider))]
    public abstract class InteractableObjectBase : MonoBehaviour
    {
        public abstract void Interact(Player sender);
    }
}