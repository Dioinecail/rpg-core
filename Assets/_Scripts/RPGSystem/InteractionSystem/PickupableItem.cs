﻿namespace RPG.InteractionSystem
{
    using RPG.Core;
    using RPG.Items;

    public class PickupableItem : InteractableObjectBase
    {
        private Item targetItem;



        public void CreateItem(Item target)
        {
            //targetItem = ItemUtility.CreateItem(target);
        }

        public override void Interact(Player sender)
        {
            sender.Inventory.PickUpItem(targetItem);
            PickUp();
        }

        private void PickUp()
        {
            gameObject.SetActive(false);
        }
    }
}