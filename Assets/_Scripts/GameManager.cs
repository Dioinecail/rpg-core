﻿using RPG.Core;
using RPG.PlayerControls;
using RPG.LevelManagement;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public static System.Random mainRandom;

    public static event Action<int, Player> onPlayerCreatedEvent;

    [SerializeField] private LevelBuilder levelBuilder;
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private int maxPlayers = 4;
    public bool generateRandomSeed;

    private Player[] players;
    private Vector3 startingPosition;

    

    public Player GetPlayer(int index)
    {
        if(players != null)
        {
            return players[index];
        }

        return null;
    }

    public Player CreatePlayer(int index, CharacterClass selectedClass)
    {
        if(players[index] != null)
        {
            ClearPlayer(index);
        }

        GameObject newPlayer = Instantiate(playerPrefab, startingPosition, Quaternion.identity);
        Player p = newPlayer.GetComponent<Player>();
        p.Init(selectedClass);

        players[index] = p;

        PlayerAbstractBehaviour[] playerBehaviours = newPlayer.GetComponents<PlayerAbstractBehaviour>();

        for (int i = 0; i < playerBehaviours.Length; i++)
        {
            playerBehaviours[i].Init(p);
        }

        onPlayerCreatedEvent?.Invoke(index, p);

        return p;
    }

    public void CreateNewPlayer(CharacterClass selectedClass)
    {
        for (int i = 0; i < maxPlayers; i++)
        {
            if (players[i] != null)
                continue;
            else
            {
                CreatePlayer(i, selectedClass);
                return;
            }
        }

    }

    private void Awake()
    {
        Instance = this;

        players = new Player[maxPlayers];

        PlayerPrefsManager.ResetAllPrefs();

        transform.SetParent(null);
        DontDestroyOnLoad(gameObject);
    }

    private void ClearPlayer(int index)
    {
        Destroy(players[index].gameObject);
        players[index] = null;
    }

    private void OnLevelCreated(Vector3 entrancePosition)
    {
        startingPosition = entrancePosition;
    }

    private void OnEnable()
    {
        levelBuilder.onLevelGeneratedEvent += OnLevelCreated;

        if (generateRandomSeed)
            levelBuilder.seed = UnityEngine.Random.Range(0, 100000).ToString();

        levelBuilder.GenerateLevelLayout();
        levelBuilder.FillLevelLayout();
    }

    private void OnDisable()
    {
        levelBuilder.onLevelGeneratedEvent -= OnLevelCreated;
    }
}