﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    public Transform target;

    public float powerMult;
    public float lerpSpeed;
    public float returnDuration;

    private Coroutine coroutine_Shake;



    public void Shake(float power, float duration)
    {
        if (coroutine_Shake != null)
            StopCoroutine(coroutine_Shake);

        coroutine_Shake = StartCoroutine(ShakeCoroutine(power, duration));
    }

    private IEnumerator ShakeCoroutine(float power, float duration)
    {
        float timer = 0;

        while(timer < duration)
        {
            timer += Time.deltaTime;

            float randomPositionX = Random.Range(-power * powerMult, power * powerMult);
            float randomPositionY = Random.Range(-power * powerMult, power * powerMult);

            target.localPosition = Vector3.Lerp(target.localPosition, new Vector3(randomPositionX, randomPositionY), lerpSpeed * Time.deltaTime);

            yield return null;
        }

        timer = 0;

        while(timer < returnDuration)
        {
            timer += Time.deltaTime;

            target.localPosition = Vector3.Lerp(target.localPosition, Vector3.zero, timer / returnDuration);
            yield return null;
        }
    }
}