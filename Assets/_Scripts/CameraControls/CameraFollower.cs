﻿using RPG.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public float speedX;
    public float speedY;
    public Vector2 offset;

    private Transform target;



    private void FixedUpdate()
    {
        if(target != null)
        {
                FollowTarget();
        }
        else
        {
            LookForPlayer();
        }
    }

    private void FollowTarget()
    {
        Vector3 intendedPosition = target.position;

        intendedPosition.z = transform.position.z;
        intendedPosition.x = Mathf.Lerp(transform.position.x, intendedPosition.x + offset.x, speedX * Time.fixedDeltaTime);
        intendedPosition.y = Mathf.Lerp(transform.position.y, intendedPosition.y + offset.y, speedY * Time.fixedDeltaTime);

        transform.position = intendedPosition;
    }

    private void LookForPlayer()
    {
        Player p = GameManager.Instance.GetPlayer(0);
        if (p != null)
            target = p.gameObject.transform;
    }
}
