﻿namespace RPG.Input
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.InputSystem;

    public enum InputState
    {
        Menu,
        Gameplay
    }

    public class InputManager : MonoBehaviour
    {
        public static InputManager Instance;

        #region MENU INPUTS

        public static event Action onMenuButtonUpPressedEvent;
        public static event Action onMenuButtonDownPressedEvent;
        public static event Action onMenuButtonRightPressedEvent;
        public static event Action onMenuButtonLeftPressedEvent;
        public static event Action onMenuButtonPageLeftPressedEvent;
        public static event Action onMenuButtonPageRightPressedEvent;
        public static event Action onMenuButtonConfirmPressedEvent;
        public static event Action onMenuButtonCancelPressedEvent;
        public static event Action onMenuButtonUsePressedEvent;
        public static event Action onMenuButtonThrowPressedEvent;

        public static event Action onMenuSelectButtonPressedEvent;
        public static event Action onMenuStartButtonPressedEvent;

        #endregion


        #region GAMEPLAY MAPPING

        // directions inputs
        public static event Action<float> onHorizontalInputsEvent;
        public static event Action<float> onVerticalInputsEvent;

        // main gameplay inputs
        public static event Action onJumpButtonPressedEvent;
        public static event Action onJumpButtonUnpressedEvent;
        public static event Action onMainAttackButtonPressedEvent;
        public static event Action onMainAttackButtonUnpressedEvent;
        public static event Action onSkillAttackButtonPressedEvent;
        public static event Action onSkillAttackButtonUnpressedEvent;
        public static event Action onMagicButtonPressedEvent;
        public static event Action onMagicButtonUnpressedEvent;

        // secondary gameplay inputs
        public static event Action onInteractButtonPressedEvent;
        public static event Action onSecondaryButtonPressedEvent;
        public static event Action onGameplayMenuButtonPressedEvent;
        public static event Action onInventoryButtonPressedEvent;

        // menu inputs
        public static event Action onChangeSpellLeftButtonPressedEvent;
        public static event Action onChangeSpellRightButtonPressedEvent;

        #endregion

        public PlayerInput targetInput;
        public InputState currentInputState = InputState.Menu;

        private float lastHorizontalInputValue;
        private float lastVerticalInputValue;

        private InputControlActions inputControls;



        private void Awake()
        {
            Instance = this;

            inputControls = new InputControlActions();
            inputControls.Enable();

            SetNewInputCallbacks();
        }

        private void Update()
        {
            switch (currentInputState)
            {
                case InputState.Menu:
                    ProcessMenuInputs();
                    break;
                case InputState.Gameplay:
                    ProcessGameplayInputs();
                    break;
                default:
                    break;
            }
        }

        public void ChangeInputState(InputState newState)
        {
            RemoveInputCallbacks();

            currentInputState = newState;

            SetNewInputCallbacks();
        }

        public void ChangeInputState(string inputState)
        {
            switch (inputState)
            {
                case "Menu":
                    ChangeInputState(InputState.Menu);
                    break;
                case "Gameplay":
                    ChangeInputState(InputState.Gameplay);
                    break;
                default:
                    break;
            }
        }

        private void RemoveInputCallbacks()
        {
            switch (currentInputState)
            {
                case InputState.Menu:
                    inputControls.UI.Confirm.started -= MenuButtonConfirmPressed;
                    inputControls.UI.Cancel.started -= MenuButtonCancelPressed;
                    inputControls.UI.PageLeft.started -= MenuButtonPageLeftPressed;
                    inputControls.UI.PageRight.started -= MenuButtonPageRightPressed;
                    inputControls.UI.StartButton.started -= MenuStartButtonPageLeftPressed;
                    inputControls.UI.SelectButton.started -= MenuSelectButtonPageRightPressed;
                    inputControls.UI.UseButton.started -= MenuButtonUsePressed;
                    inputControls.UI.ThrowButton.started -= MenuButtonThrowPressed;
                    break;
                case InputState.Gameplay:
                    inputControls.Gameplay.Jump.started -= JumpButtonPressed;
                    inputControls.Gameplay.Jump.canceled -= JumpButtonUnpressed;
                    inputControls.Gameplay.MainAttack.started -= MainAttackButtonPressed;
                    inputControls.Gameplay.MainAttack.canceled -= MainAttackButtonUnpressed;
                    inputControls.Gameplay.SkillAttack.started -= SkillAttackButtonPressed;
                    inputControls.Gameplay.SkillAttack.canceled -= SkillAttackButtonUnpressed;
                    inputControls.Gameplay.Dodge.started -= DodgeButtonPressed;
                    inputControls.Gameplay.Dodge.canceled -= DodgeButtonUnpressed;
                    inputControls.Gameplay.Interact.started -= InteractButtonPressed;
                    inputControls.Gameplay.SecondaryAction.started -= SecondaryButtonPressed;
                    inputControls.Gameplay.ChangeSpellLeft.started -= ChangeSpellLeftButtonPressed;
                    inputControls.Gameplay.ChangeSpellRight.started -= ChangeSpellRightButtonPressed;
                    inputControls.Gameplay.StartButton.started -= StartButtonPressed;
                    inputControls.Gameplay.SelectButton.started -= SelectButtonPressed;
                    break;
                default:
                    break;
            }
        }

        private void SetNewInputCallbacks()
        {
            inputControls.UI.Disable();
            inputControls.Gameplay.Disable();

            switch (currentInputState)
            {
                case InputState.Menu:
                    targetInput.SwitchCurrentActionMap("UI");
                    inputControls.UI.Enable();
                    inputControls.UI.Confirm.started += MenuButtonConfirmPressed;
                    inputControls.UI.Cancel.started += MenuButtonCancelPressed;
                    inputControls.UI.PageLeft.started += MenuButtonPageLeftPressed;
                    inputControls.UI.PageRight.started += MenuButtonPageRightPressed;
                    inputControls.UI.StartButton.started += MenuStartButtonPageLeftPressed;
                    inputControls.UI.SelectButton.started += MenuSelectButtonPageRightPressed;
                    inputControls.UI.UseButton.started += MenuButtonUsePressed;
                    inputControls.UI.ThrowButton.started += MenuButtonThrowPressed;
                    break;
                case InputState.Gameplay:
                    targetInput.SwitchCurrentActionMap("Gameplay");
                    inputControls.Gameplay.Enable();
                    inputControls.Gameplay.Jump.started += JumpButtonPressed;
                    inputControls.Gameplay.Jump.canceled += JumpButtonUnpressed;
                    inputControls.Gameplay.MainAttack.started += MainAttackButtonPressed;
                    inputControls.Gameplay.MainAttack.canceled += MainAttackButtonUnpressed;
                    inputControls.Gameplay.SkillAttack.started += SkillAttackButtonPressed;
                    inputControls.Gameplay.SkillAttack.canceled += SkillAttackButtonUnpressed;
                    inputControls.Gameplay.Dodge.started += DodgeButtonPressed;
                    inputControls.Gameplay.Dodge.canceled += DodgeButtonUnpressed;
                    inputControls.Gameplay.Interact.started += InteractButtonPressed;
                    inputControls.Gameplay.SecondaryAction.started += SecondaryButtonPressed;
                    inputControls.Gameplay.ChangeSpellLeft.started += ChangeSpellLeftButtonPressed;
                    inputControls.Gameplay.ChangeSpellRight.started += ChangeSpellRightButtonPressed;
                    inputControls.Gameplay.StartButton.started += StartButtonPressed;
                    inputControls.Gameplay.SelectButton.started += SelectButtonPressed;
                    break;
                default:
                    break;
            }
        }

        #region MENU BUTTONS PROCESSING

        private void MenuButtonConfirmPressed(InputAction.CallbackContext obj)
        {
            onMenuButtonConfirmPressedEvent?.Invoke();
        }

        private void MenuButtonCancelPressed(InputAction.CallbackContext obj)
        {
            onMenuButtonCancelPressedEvent?.Invoke();
        }

        private void MenuButtonUsePressed(InputAction.CallbackContext obj)
        {
            onMenuButtonUsePressedEvent?.Invoke();
        }

        private void MenuButtonThrowPressed(InputAction.CallbackContext obj)
        {
            onMenuButtonThrowPressedEvent?.Invoke();
        }

        private void MenuButtonPageLeftPressed(InputAction.CallbackContext obj)
        {
            onMenuButtonPageLeftPressedEvent?.Invoke();
        }

        private void MenuButtonPageRightPressed(InputAction.CallbackContext obj)
        {
            onMenuButtonPageRightPressedEvent?.Invoke();
        }

        private void MenuStartButtonPageLeftPressed(InputAction.CallbackContext obj)
        {
            onMenuStartButtonPressedEvent?.Invoke();
        }

        private void MenuSelectButtonPageRightPressed(InputAction.CallbackContext obj)
        {
            onMenuSelectButtonPressedEvent?.Invoke();
        }

        #endregion

        #region GAMEPLAY BUTTONS PROCESSING

        private void JumpButtonPressed(InputAction.CallbackContext obj)
        {
            onJumpButtonPressedEvent?.Invoke();
        }

        private void JumpButtonUnpressed(InputAction.CallbackContext obj)
        {
            onJumpButtonUnpressedEvent?.Invoke();
        }

        private void MainAttackButtonPressed(InputAction.CallbackContext obj)
        {
            onMainAttackButtonPressedEvent?.Invoke();
        }

        private void MainAttackButtonUnpressed(InputAction.CallbackContext obj)
        {
            onMainAttackButtonUnpressedEvent?.Invoke();
        }

        private void SkillAttackButtonPressed(InputAction.CallbackContext obj)
        {
            onMagicButtonPressedEvent?.Invoke();
        }

        private void SkillAttackButtonUnpressed(InputAction.CallbackContext obj)
        {
            onMagicButtonUnpressedEvent?.Invoke();
        }

        private void DodgeButtonPressed(InputAction.CallbackContext obj)
        {
            onSkillAttackButtonPressedEvent?.Invoke();
        }

        private void DodgeButtonUnpressed(InputAction.CallbackContext obj)
        {
            onSkillAttackButtonUnpressedEvent?.Invoke();
        }

        private void InteractButtonPressed(InputAction.CallbackContext obj)
        {
            onInteractButtonPressedEvent?.Invoke();
        }

        private void SecondaryButtonPressed(InputAction.CallbackContext obj)
        {
            onSecondaryButtonPressedEvent?.Invoke();
        }

        private void ChangeSpellLeftButtonPressed(InputAction.CallbackContext obj)
        {
            onChangeSpellLeftButtonPressedEvent?.Invoke();
        }

        private void ChangeSpellRightButtonPressed(InputAction.CallbackContext obj)
        {
            onChangeSpellRightButtonPressedEvent?.Invoke();
        }

        private void StartButtonPressed(InputAction.CallbackContext obj)
        {
            onGameplayMenuButtonPressedEvent?.Invoke();
        }

        private void SelectButtonPressed(InputAction.CallbackContext obj)
        {
            onInventoryButtonPressedEvent?.Invoke();
        }

        #endregion

        private void ProcessMenuInputs()
        {
            Vector2 directionalInputs = inputControls.UI.Navigate.ReadValue<Vector2>();

            float currentHorizontalInputValue = directionalInputs.x;
            float currentVerticalInputValue = directionalInputs.y;

            if (currentHorizontalInputValue > 0.1f && (lastHorizontalInputValue < 0.1f))
                onMenuButtonRightPressedEvent?.Invoke();

            if (currentHorizontalInputValue < -0.1f && (lastHorizontalInputValue > -0.1f))
                onMenuButtonLeftPressedEvent?.Invoke();

            if (currentVerticalInputValue > 0.1f && (lastVerticalInputValue < 0.1f))
                onMenuButtonUpPressedEvent?.Invoke();

            if (currentVerticalInputValue < -0.1f && (lastVerticalInputValue > -0.1f))
                onMenuButtonDownPressedEvent?.Invoke();

            lastHorizontalInputValue = currentHorizontalInputValue;
            lastVerticalInputValue = currentVerticalInputValue;
        }

        private void ProcessGameplayInputs()
        {
            Vector2 directionalInputs = inputControls.Gameplay.Movement.ReadValue<Vector2>();            

            float currentHorizontalInputValue = directionalInputs.x;
            float currentVerticalInputValue = directionalInputs.y;

            onHorizontalInputsEvent?.Invoke(currentHorizontalInputValue);
            onVerticalInputsEvent?.Invoke(currentVerticalInputValue);
        }

        private void LoadSavedButtonMappings()
        {
            // TODO: Add loading of previously saved button mappings
        }

        private void SaveButtonMappings()
        {
            // TODO: Add saving current button mappings
        }

        private void OnEnable()
        {
            inputControls.Enable();
        }

        private void OnDisable()
        {
            inputControls.Disable();
        }
    }
}