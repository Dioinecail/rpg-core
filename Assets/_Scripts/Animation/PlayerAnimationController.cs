﻿namespace RPG.Animation
{
    using UnityEngine;
    using RPG.PlayerControls;
    using System;

    public class PlayerAnimationController : MonoBehaviour
    {
        [SerializeField] private Animator anim;
        [SerializeField] private Transform targetModel;

        private PlayerMovementBehaviour movementBehaviour;
        private PlayerMeleeAttackBehaviour meleeBehaviour;
        private PlayerGrabWallBehaviour grabWallBehaviour;
        private PlayerInteractBehaviour interactBehaviour;
        private CollisionBehaviour collisionBehaviour;



        private void Awake()
        {
            movementBehaviour = GetComponent<PlayerMovementBehaviour>();
            meleeBehaviour = GetComponent<PlayerMeleeAttackBehaviour>();
            grabWallBehaviour = GetComponent<PlayerGrabWallBehaviour>();
            interactBehaviour = GetComponent<PlayerInteractBehaviour>();
            collisionBehaviour = GetComponent<CollisionBehaviour>();
        }

        private void PlayState(string state)
        {
            anim.Play(state, 0);
        }

        private void SetFloat(string parameterName, float value)
        {
            anim.SetFloat(parameterName, value);
        }

        private void SetBool(string parameterName, bool value)
        {
            anim.SetBool(parameterName, value);
        }

        private void SetTrigger(string triggerName)
        {
            anim.SetTrigger(triggerName);
        }

        private void ResetTrigger(string triggerName)
        {
            anim.ResetTrigger(triggerName);
        }

        private void OnMovement(float speed)
        {
            if(Mathf.Abs(speed) > 0.01f)
            {
                int direction = (int)Mathf.Sign(speed);

                Vector3 directionVector = Vector3.right * direction;
                Quaternion rotation = Quaternion.LookRotation(directionVector, Vector3.up);

                targetModel.rotation = rotation;
            }

            SetFloat("VelocityX", Mathf.Abs(speed));
        }

        private void OnJumpStart()
        {
            PlayState("Jump");
        }

        private void OnJumpStop()
        {

        }

        private void OnMelee()
        {
            PlayState("MeleeAttack");
        }

        private void OnGroundStateChanged(bool state)
        {
            SetBool("OnGround", state);
        }

        private void OnEnable()
        {
            movementBehaviour.onMovementEvent += OnMovement;
            movementBehaviour.onJumpStartEvent += OnJumpStart;
            movementBehaviour.onJumpStopEvent += OnJumpStop;
            meleeBehaviour.onAttackEvent += OnMelee;
            collisionBehaviour.onGroundStateChanged += OnGroundStateChanged;
        }

        private void OnDisable()
        {
            movementBehaviour.onMovementEvent -= OnMovement;
            movementBehaviour.onJumpStartEvent -= OnJumpStart;
            movementBehaviour.onJumpStopEvent -= OnJumpStop;
            meleeBehaviour.onAttackEvent -= OnMelee;
            collisionBehaviour.onGroundStateChanged -= OnGroundStateChanged;
        }
    }
}