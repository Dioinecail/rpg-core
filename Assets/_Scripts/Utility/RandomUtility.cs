﻿namespace Utility
{
    using System.Collections.Generic;
    using System;

    public static class RandomUtility
    {
        public static T GetRandom<T>(this List<T> list, bool remove = false, System.Random seed = null)
        {
            return list.GetRandom(0, list.Count, remove, seed);
        }

        public static T GetRandom<T>(this List<T> list, int min, int max, bool remove = false, Random seed = null)
        {
            if (list.Count == 0)
                return default(T);

            if (seed == null)
                seed = new Random();

            int rnd = seed.Next(min, max);

            T item = list[rnd];

            if (remove)
                list.RemoveAt(rnd);

            return item;
        }

        public static T GetRandom<T>(this T[] array, Random seed = null)
        {
            return array.GetRandom(0, array.Length, seed);
        }

        public static T GetRandom<T>(this T[] array, int min, int max, Random seed = null)
        {
            if (seed == null)
                seed = new Random();

            int rnd = seed.Next(min, max);

            return array[rnd];
        }
    }
}