﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPositionUtility : MonoBehaviour
{
    public GameObject prefab;

    public float step = 1;

    public int amount = 10;



    [ContextMenu("Randomize")]
    public void Randomize()
    {
        for (int a = 0; a < amount; a++)
        {
            GameObject copy = Instantiate(prefab, prefab.transform.position + Vector3.right * step * a, Quaternion.identity);
        }
    }
}