﻿namespace RPG.PlayerControls
{
    using RPG.Input;
    using RPG.InteractionSystem;
    using UnityEngine;

    public class PlayerInteractBehaviour : PlayerAbstractBehaviour
    {
        public Vector2 interactionOffset;
        public float interactionRange;
        public LayerMask interactionMask;

        private Collider[] interactionBuffer = new Collider[1];

        

        private void OnInteractButtonPressed()
        {
            if (!isAvailable)
                return;

            ClearBuffer();
            TryInteract();
        }

        private void ClearBuffer()
        {
            interactionBuffer[0] = null;
        }

        private void TryInteract()
        {
            Physics.OverlapSphereNonAlloc(cachedTransform.position + (Vector3)interactionOffset, interactionRange, interactionBuffer, interactionMask, QueryTriggerInteraction.Collide);

            if(interactionBuffer[0] != null)
            {
                InteractableObjectBase foundObject = interactionBuffer[0].GetComponent<InteractableObjectBase>();

                foundObject?.Interact(playerEntity);
            }
        }

        private void OnEnable()
        {
            InputManager.onInteractButtonPressedEvent += OnInteractButtonPressed;
        }

        private void OnDisable()
        {
            InputManager.onInteractButtonPressedEvent -= OnInteractButtonPressed;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position + (Vector3)interactionOffset, interactionRange);
        }
    }
}