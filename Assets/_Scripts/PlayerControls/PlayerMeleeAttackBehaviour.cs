﻿namespace RPG.PlayerControls
{
    using System.Collections;
    using UnityEngine;
    using RPG.Input;
    using RPG.Equipment;
    using RPG.Damage;
    using System;
    using RPG.Core;

    public class PlayerMeleeAttackBehaviour : PlayerAbstractBehaviour
    {
        public event Action onAttackEvent;

        public Vector2 attackOffset;
        public float attackSpeedBase;
        public float attackRangeBase;

        public LayerMask hitMask;

        private Collider[] hitBuffer = new Collider[16];
        private Coroutine coroutine_attackCooldown;
        private object currentlyEquippedWeapon;
        private bool isAttackAvailable = true;
        private bool isGrabbingLedge = false;
        private int currentDirection = 1;



        public override void Init(Player entity)
        {
            base.Init(entity);
            entity.Equipment.onItemEquipped += OnItemEquipped;
        }

        private void OnMeleeAttackButtonPressed()
        {
            if (coroutine_attackCooldown != null)
                return;

            coroutine_attackCooldown = StartCoroutine(CoroutineAttackCooldown());

            OnAttack();
        }

        private void OnAttack()
        {
            ClearBuffer();
            AttemptAttack();

            onAttackEvent?.Invoke();
        }

        private void ClearBuffer()
        {
            for (int i = 0; i < hitBuffer.Length; i++)
            {
                hitBuffer[i] = null;
            }
        }

        private void AttemptAttack()
        {
            float attackRange = attackRangeBase;

            //if (currentlyEquippedWeapon != null)
            //    attackRange += currentlyEquippedWeapon.GetAttackRange();

            Vector3 currentOffset = attackOffset;
            currentOffset.x *= currentDirection;

            Physics.OverlapSphereNonAlloc(cachedTransform.position + currentOffset, attackRange, hitBuffer, hitMask, QueryTriggerInteraction.Collide);

            for (int i = 0; i < hitBuffer.Length; i++)
            {
                if(hitBuffer[i] != null)
                {
                    GameEntity destructableObject = hitBuffer[i].gameObject.GetComponent<GameEntity>();

                    if(destructableObject != null)
                    {
                        int damage = DamageSystem.CalculatePhysicalMeleeDamage(playerEntity, destructableObject);
                        destructableObject.DealDamage(damage);
                    }
                }
            }
        }

        private void OnHorizontalInput(float input)
        {
            if(Mathf.Abs(input) > 0.05f)
            {
                currentDirection = (int)Mathf.Sign(input);
            }
        }

        private void OnItemEquipped(ItemEquippedEventArgs args)
        {
            //if(args.itemUnequipped != null && args.itemUnequipped.ItemReferense is WeaponItem)
            //{
            //    WeaponItem oldItem = args.itemUnequipped.ItemReferense as WeaponItem;

            //    if (oldItem.GetCategory().Equals(EquipmentCategory.WEAPON))
            //        currentlyEquippedWeapon = null;
            //}

            //if(args.itemEquipped != null && args.itemEquipped.ItemReferense is WeaponItem)
            //{
            //    WeaponItem newItem = args.itemEquipped.ItemReferense as WeaponItem;

            //    if(newItem.GetCategory().Equals(EquipmentCategory.WEAPON))
            //        currentlyEquippedWeapon = args.itemEquipped.ItemReferense as WeaponItem;

            //}
        }

        private float GetWeaponAttackSpeed()
        {
            //if (currentlyEquippedWeapon != null)
            //    return currentlyEquippedWeapon.GetAttackSpeed();
            //else
                return 0;
        }

        private IEnumerator CoroutineAttackCooldown()
        {
            float attackSpeed = attackSpeedBase + GetWeaponAttackSpeed();

            yield return new WaitForSeconds(1 / attackSpeed);

            coroutine_attackCooldown = null;
        }

        private void OnEnable()
        {
            InputManager.onMainAttackButtonPressedEvent += OnMeleeAttackButtonPressed;
            InputManager.onHorizontalInputsEvent += OnHorizontalInput;
            cachedTransform = transform;
        }

        private void OnDisable()
        {
            InputManager.onHorizontalInputsEvent -= OnHorizontalInput;
            InputManager.onMainAttackButtonPressedEvent -= OnMeleeAttackButtonPressed;
            playerEntity.Equipment.onItemEquipped -= OnItemEquipped;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;

            float currentRange = attackRangeBase;

            //if (currentlyEquippedWeapon != null)
            //    currentRange += currentlyEquippedWeapon.GetAttackRange();

            Vector3 currentOffset = attackOffset;
            currentOffset.x *= currentDirection;

            Gizmos.DrawWireSphere(transform.position + currentOffset, attackRangeBase);
        }
    }
}