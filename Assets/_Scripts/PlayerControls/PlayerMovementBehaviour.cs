﻿namespace RPG.PlayerControls
{
    using RPG.Core;
    using RPG.Input;
    using System;
    using System.Collections;
    using UnityEngine;

    public class PlayerMovementBehaviour : PlayerAbstractBehaviour
    {
        public Action<float> onMovementEvent;
        public Action onJumpStartEvent;
        public Action onJumpStopEvent;

        public PlayerGrabWallBehaviour grabLedgeBehaviour;
        public CollisionBehaviour collisionBehaviour;

        public float baseMovementSpeed;
        public float movementSpeedPerSPD;

        public float jumpHeight;
        public float jumpStopSpeed;

        public float safeAirTime;

        private bool OnGround { get { return collisionBehaviour.IsOnGround; } }
        private bool jumpButtonHeldDown;

        private Rigidbody rBody;
        private Coroutine coroutine_stopJump;
        private Coroutine coroutine_safeAir;

        private bool isJumpAvailable = true;
        private bool isGrabbingLedge = false;
        private float verticalInput;



        public override void Init(Player entity)
        {
            base.Init(entity);
            rBody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            if(isGrabbingLedge)
            {
                Vector3 velocity = rBody.velocity;
                velocity.y = 0;
                rBody.velocity = velocity;
            }
        }

        private void Jump()
        {
            float jumpSpeed;

            if(isGrabbingLedge)
            {
                grabLedgeBehaviour.SetState(true);
                grabLedgeBehaviour.LetGoOfLedge();
                isGrabbingLedge = false;

                if (verticalInput < 0)
                    jumpSpeed = 0;
                else
                    jumpSpeed = CalculateJumpVelocity();
            }
            else
            {
                jumpSpeed = CalculateJumpVelocity();
            }

            rBody.isKinematic = false;
            rBody.velocity = new Vector3(rBody.velocity.x, jumpSpeed);

            onJumpStartEvent?.Invoke();
        }

        private void OnHorizontalInput(float value)
        {
            if (isGrabbingLedge)
                return;

            Vector3 velocity = rBody.velocity;
            velocity.x = value * (baseMovementSpeed + (movementSpeedPerSPD * playerEntity.GetTotalStat(Stat.SPD)));
            rBody.velocity = velocity;

            onMovementEvent?.Invoke(rBody.velocity.x);
        }

        private void OnVerticalInput(float value)
        {
            verticalInput = value;
        }

        private void OnJumpButtonPressed()
        {
            jumpButtonHeldDown = true;

            if (!isJumpAvailable)
                return;

            if (OnGround || isGrabbingLedge || coroutine_safeAir != null)
            {
                Jump();
                ResetSafeAirTimer();
            }
        }

        private void OnJumpButtonUnpressed()
        {
            jumpButtonHeldDown = false;

            if (coroutine_stopJump != null)
                StopCoroutine(coroutine_stopJump);

            coroutine_stopJump = StartCoroutine(StopJumping());
        }

        private void OnGroundStateChanged(bool state)
        {
            if (!state && !jumpButtonHeldDown)
            {
                coroutine_safeAir = StartCoroutine(SafeAirTimer());
            }
            else
            {
                ResetSafeAirTimer();
            }
        }

        private IEnumerator StopJumping()
        {
            onJumpStopEvent?.Invoke();

            while (rBody.velocity.y > 0)
            {
                float currentVerticalVelocity = rBody.velocity.y;

                currentVerticalVelocity = Mathf.Lerp(currentVerticalVelocity, 0, jumpStopSpeed * Time.deltaTime);

                rBody.velocity = new Vector3(rBody.velocity.x, currentVerticalVelocity);

                yield return null;
            }

            coroutine_stopJump = null;
        }

        private IEnumerator SafeAirTimer()
        {
            yield return new WaitForSeconds(safeAirTime);

            coroutine_safeAir = null;
        }

        private void ResetSafeAirTimer()
        {
            if (coroutine_safeAir != null)
            {
                StopCoroutine(coroutine_safeAir);
                coroutine_safeAir = null;
            }
        }

        private float CalculateJumpVelocity()
        {
            return Mathf.Sqrt(Mathf.Abs(jumpHeight * Physics.gravity.y * 2));
        }

        private void OnLedgeGrabbed()
        {
            rBody.isKinematic = true;
            rBody.velocity = Vector3.zero;
            grabLedgeBehaviour.SetState(false);
            isGrabbingLedge = true;
        }

        private void OnEnable()
        {
            grabLedgeBehaviour.onLedgeGrabbed += OnLedgeGrabbed;
            InputManager.onHorizontalInputsEvent += OnHorizontalInput;
            InputManager.onVerticalInputsEvent += OnVerticalInput;
            InputManager.onJumpButtonPressedEvent += OnJumpButtonPressed;
            InputManager.onJumpButtonUnpressedEvent += OnJumpButtonUnpressed;
            collisionBehaviour.onGroundStateChanged += OnGroundStateChanged;
        }

        private void OnDisable()
        {
            grabLedgeBehaviour.onLedgeGrabbed -= OnLedgeGrabbed;
            InputManager.onHorizontalInputsEvent -= OnHorizontalInput;
            InputManager.onVerticalInputsEvent -= OnVerticalInput;
            InputManager.onJumpButtonPressedEvent -= OnJumpButtonPressed;
            InputManager.onJumpButtonUnpressedEvent -= OnJumpButtonUnpressed;
            collisionBehaviour.onGroundStateChanged -= OnGroundStateChanged;
        }
    }
}