﻿namespace RPG.PlayerControls
{
    using RPG.Core;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class PlayerAbstractBehaviour : MonoBehaviour
    {
        protected bool isAvailable = true;
        protected Player playerEntity;
        protected Transform cachedTransform;



        public virtual void SetState(bool isActive)
        {
            isAvailable = isActive;
        }

        public virtual void Init(Player entity)
        {
            this.playerEntity = entity;
            cachedTransform = transform;
        }
    }
}