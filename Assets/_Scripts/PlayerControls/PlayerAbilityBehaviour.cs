﻿namespace RPG.PlayerControls
{
    using RPG.Input;
    using RPG.Abilities;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using RPG.Core;
    using System;

    public class PlayerAbilityBehaviour : PlayerAbstractBehaviour
    {
        private event Action onAbilityUpdate;

        private Dictionary<AbilityBase, Coroutine> coroutines_Cooldown;
        private AbilityBase selectedAbility;



        public override void Init(Player entity)
        {
            base.Init(entity);
            coroutines_Cooldown = new Dictionary<AbilityBase, Coroutine>();
            OnAbilityChanged(entity.Abilities[0]);
            entity.Abilities.onSelectedAbilityChanged += OnAbilityChanged;
        }

        private void Update()
        {
            onAbilityUpdate?.Invoke();
        }

        #region HANDLE ABILITIES

        private bool TryCastAbility()
        {
            AbilityInstance newInstance = selectedAbility.Cast(transform.position, Quaternion.LookRotation(Vector3.right, Vector3.up), playerEntity, null);

            if(newInstance != null)
            {
                onAbilityUpdate += newInstance.Update;

                int manaCost = selectedAbility.GetCost(playerEntity.GetStats());
                playerEntity.Mana.DecreaseCurrent(manaCost);

                return true;
            }

            return false;
        }

        private IEnumerator CoroutineAbilityCooldown(AbilityBase ability)
        {
            yield return new WaitForSeconds(ability.GetCooldown(playerEntity.GetStats()));

            coroutines_Cooldown.Remove(ability);
        }

        #endregion

        #region HANDLE INPUTS

        private void OnSkillAttackButtonPressed()
        {
            if (coroutines_Cooldown.ContainsKey(selectedAbility))
                return;

            if(TryCastAbility())
                coroutines_Cooldown.Add(selectedAbility, StartCoroutine(CoroutineAbilityCooldown(selectedAbility)));
        }

        private void OnAbilityChanged(AbilityBase newAbility)
        {
            selectedAbility = newAbility;
        }

        private void OnEnable()
        {
            InputManager.onSkillAttackButtonPressedEvent += OnSkillAttackButtonPressed;
        }

        private void OnDisable()
        {
            InputManager.onSkillAttackButtonPressedEvent -= OnSkillAttackButtonPressed;
            playerEntity.Abilities.onSelectedAbilityChanged -= OnAbilityChanged;
        }

        #endregion
    }
}