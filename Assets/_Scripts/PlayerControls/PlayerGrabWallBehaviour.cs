﻿namespace RPG.PlayerControls
{
    using RPG.Input;
    using System;
    using System.Collections;
    using UnityEngine;

    public class PlayerGrabWallBehaviour : PlayerAbstractBehaviour
    {
        public event Action onLedgeGrabbed;

        public CollisionBehaviour collisionBehaviour;
        public LayerMask ledgeMask;
        public Vector2 ledgeGrabOffset;
        public Vector2 ledgeUpperBoundCheckOffset;
        public float ledgeGrabDistance;
        public float ledgeGrabCooldown = 0.25f;

        private float currentGrabXPosition;
        private RaycastHit[] ledgeBuffer = new RaycastHit[1];
        private RaycastHit[] ledgeUpperBoundBuffer = new RaycastHit[1];
        private bool isGrabbingLedge = false;

        private Coroutine coroutine_grabLedgeCooldown;
        private bool isGrounded;



        public void LetGoOfLedge()
        {
            isGrabbingLedge = false;

            if (coroutine_grabLedgeCooldown != null)
                StopCoroutine(coroutine_grabLedgeCooldown);

            coroutine_grabLedgeCooldown = StartCoroutine(GrabCooldown());
        }

        private void Update()
        {
            if(coroutine_grabLedgeCooldown == null && isAvailable && !isGrounded)
            {
                ClearBuffers();
                CheckLedge();
            }
        }

        private void CheckLedge()
        {
            Vector2 grabPositionCheck = ledgeGrabOffset;
            grabPositionCheck.x += cachedTransform.position.x;
            grabPositionCheck.y += cachedTransform.position.y;

            Vector2 upperBoundPositionCheck = ledgeUpperBoundCheckOffset;
            upperBoundPositionCheck.x += cachedTransform.position.x;
            upperBoundPositionCheck.y += cachedTransform.position.y;

            Physics.RaycastNonAlloc(grabPositionCheck, Vector3.right * currentGrabXPosition, ledgeBuffer, ledgeGrabDistance, ledgeMask, QueryTriggerInteraction.Ignore);
            Physics.RaycastNonAlloc(upperBoundPositionCheck, Vector3.right * currentGrabXPosition, ledgeUpperBoundBuffer, ledgeGrabDistance, ledgeMask, QueryTriggerInteraction.Ignore);

            if (ledgeBuffer[0].collider != null && ledgeUpperBoundBuffer[0].collider == null)
            {
                // Ledge found, grab it!
                if(!isGrabbingLedge)
                {
                    isGrabbingLedge = true;
                    onLedgeGrabbed?.Invoke();
                }
            }
        }

        private void ClearBuffers()
        {
            ledgeBuffer[0] = new RaycastHit();
            ledgeUpperBoundBuffer[0] = new RaycastHit();
        }

        private void OnHorizontalInput(float value)
        {
            currentGrabXPosition = value;
        }

        private IEnumerator GrabCooldown()
        {
            yield return new WaitForSeconds(ledgeGrabCooldown);

            coroutine_grabLedgeCooldown = null;
        }

        private void OnGroundStateChanged(bool state)
        {
            isGrounded = state;
        }

        private void OnEnable()
        {
            cachedTransform = transform;
            collisionBehaviour.onGroundStateChanged += OnGroundStateChanged;
            InputManager.onHorizontalInputsEvent += OnHorizontalInput;
        }

        private void OnDisable()
        {
            collisionBehaviour.onGroundStateChanged -= OnGroundStateChanged;
            InputManager.onHorizontalInputsEvent -= OnHorizontalInput;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(1, 0.5f, 1);
            Gizmos.DrawRay(transform.position + (Vector3)ledgeGrabOffset, Vector3.right * ledgeGrabDistance);

            Gizmos.color = new Color(0.5f, 0.5f, 1);
            Gizmos.DrawRay(transform.position + (Vector3)ledgeUpperBoundCheckOffset, Vector3.right * ledgeGrabDistance);
        }
    }
}