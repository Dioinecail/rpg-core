﻿namespace RPG.PlayerControls
{
    using System;
    using UnityEngine;

    public class CollisionBehaviour : MonoBehaviour
    {
        public Action<bool> onGroundStateChanged;

        public Vector3 collisionOffset;
        public LayerMask collisionMask;
        public float collisionRadius;

        private Collider[] collisionBuffer = new Collider[4];
        public bool IsOnGround { get; private set; }



        private void Update()
        {
            ClearBuffer();
            DetectCollision();
        }

        private void ClearBuffer()
        {
            for (int i = 0; i < collisionBuffer.Length; i++)
            {
                collisionBuffer[i] = null;
            }
        }

        private void DetectCollision()
        {
            Physics.OverlapSphereNonAlloc(transform.position + collisionOffset, collisionRadius, collisionBuffer, collisionMask);

            if(collisionBuffer[0] != null)
            {
                if (!IsOnGround)
                    onGroundStateChanged?.Invoke(true);

                IsOnGround = true;
            }
            else
            {
                if (IsOnGround)
                    onGroundStateChanged?.Invoke(false);

                IsOnGround = false;
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position + collisionOffset, collisionRadius);
        }
    }
}