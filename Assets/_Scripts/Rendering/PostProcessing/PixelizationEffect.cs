﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering.Universal.PostProcessing;

[System.Serializable, VolumeComponentMenu("CustomPostProcess/Pixelization")]
public class PixelizationEffect : VolumeComponent
{
    [Tooltip("Controls the amount of pixelization applied to the screen")]
    public MinFloatParameter amount = new MinFloatParameter(1, 0);
}

// Define the renderer for the custom post processing effect
[CustomPostProcess("Pixelization", CustomPostProcessInjectionPoint.AfterPostProcess)]
public class PixelizationEffectRenderer : CustomPostProcessRenderer
{
    // A variable to hold a reference to the corresponding volume component
    private PixelizationEffect m_VolumeComponent;

    // The postprocessing material
    private Material m_Material;

    // By default, the effect is visible in the scene view, but we can change that here.
    public override bool visibleInSceneView => true;



    // Initialized is called only once before the first render call
    public override void Initialize()
    {
        m_Material = CoreUtils.CreateEngineMaterial("Custom/VFX/S_PixelizationPostProcess");
    }

    // Called for each camera/injection point pair on each frame. Return true if the effect should be rendered for this camera.
    public override bool Setup(ref RenderingData renderingData, CustomPostProcessInjectionPoint injectionPoint)
    {
        // Get the current volume stack
        var stack = VolumeManager.instance.stack;
        // Get the corresponding volume component
        m_VolumeComponent = stack.GetComponent<PixelizationEffect>();
        // if blend value > 0, then we need to render this effect. 
        return m_VolumeComponent.amount.value > 0;
    }

    // The actual rendering execution is done here
    public override void Render(CommandBuffer cmd, RenderTargetIdentifier source, RenderTargetIdentifier destination, ref RenderingData renderingData, CustomPostProcessInjectionPoint injectionPoint)
    {
        if (m_Material != null)
        {
            m_Material.SetFloat("_Amount", m_VolumeComponent.amount.value);
        }
        // Since we are using a shader graph, we cann't use CoreUtils.DrawFullScreen without modifying the vertex shader.
        // So we go with the easy route and use CommandBuffer.Blit instead. The same goes if you want to use legacy image effect shaders.
        // Note: don't forget to set pass to 0 (last argument in Blit) to make sure that extra passes are not drawn.
        cmd.Blit(source, destination, m_Material, 0);
    }
}