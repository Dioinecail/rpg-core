﻿namespace RPG.LevelManagement
{
    using UnityEngine;
    using Utility;

    public enum TileType
    {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        Wall,
        Platform
    }

    [CreateAssetMenu(menuName = "LevelBlocksAsset/New", fileName = "LevelBlockAsset_")]
    public class LevelBlocksAsset : ScriptableObject
    {
        [Header("Entrance/Exit")]
        [SerializeField] private GameObject blockEntrance;
        [SerializeField] private GameObject blockExit;

        [Header("Prefab templates")]
        [SerializeField] private GameObject blockWalls;
        [SerializeField] private GameObject blockPlatforms;
        [SerializeField] private GameObject blockCornerTopLeft;
        [SerializeField] private GameObject blockCornerTopRight;
        [SerializeField] private GameObject blockCornerBottomLeft;
        [SerializeField] private GameObject blockCornerBottomRight;

        [Header("Traps")]
        [SerializeField] private GameObject[] blockTrapsFloor;
        [SerializeField] private GameObject[] blockTrapsCeiling;

        [Header("Mesh templates")]
        [SerializeField] private Mesh[] meshesWall;
        [SerializeField] private Mesh[] meshesPlatforms;
        [SerializeField] private Mesh[] meshesCornerTopLeft;
        [SerializeField] private Mesh[] meshesCornerTopRight;
        [SerializeField] private Mesh[] meshesCornerBottomLeft;
        [SerializeField] private Mesh[] meshesCornerBottomRight;

        [Header("Material")]
        [SerializeField] private Material blockMaterial;



        #region Non Hostile

        public GameObject GetEntrance() { return Instantiate(blockEntrance); }

        public GameObject GetExit() { return Instantiate(blockExit); }

        public GameObject GetTile(TileType cType, System.Random seed)
        {
            GameObject newBlock = null;
            Mesh newMesh = null;

            switch (cType)
            {
                case TileType.TopLeft:
                    newBlock = Instantiate(blockCornerTopLeft);
                    newMesh = meshesCornerTopLeft.GetRandom(seed);
                    break;
                case TileType.TopRight:
                    newBlock = Instantiate(blockCornerTopRight);
                    newMesh = meshesCornerTopRight.GetRandom(seed);
                    break;
                case TileType.BottomLeft:
                    newBlock = Instantiate(blockCornerBottomLeft);
                    newMesh = meshesCornerBottomLeft.GetRandom(seed);
                    break;
                case TileType.BottomRight:
                    newBlock = Instantiate(blockCornerBottomRight);
                    newMesh = meshesCornerBottomRight.GetRandom(seed);
                    break;
                case TileType.Wall:
                    newBlock = Instantiate(blockWalls);
                    newMesh = meshesWall.GetRandom(seed);
                    break;
                case TileType.Platform:
                    newBlock = Instantiate(blockPlatforms);
                    newMesh = meshesPlatforms.GetRandom(seed);
                    break;
                default:
                    break;
            }

            newBlock.GetComponent<MeshFilter>().sharedMesh = newMesh;
            newBlock.GetComponent<MeshRenderer>().sharedMaterial = blockMaterial;

            return newBlock;
        }

        #endregion

        #region Hostile

        public GameObject GetTrapFloor(System.Random seed) { return Instantiate(blockTrapsFloor.GetRandom(seed)); }

        public GameObject GetTrapCeiling(System.Random seed) { return Instantiate(blockTrapsCeiling.GetRandom(seed)); }

        #endregion
    }
}