﻿namespace RPG.LevelManagement
{
    using System;
    using RPG.Inventory;
    using RPG.InteractionSystem;
    using RPG.Items;
    using UnityEngine;

    [CreateAssetMenu(menuName = "LevelItemsAsset/New", fileName = "LevelItemsAsset_")]
    public class LevelItemsAsset : LevelObjectsContainerAsset<Item>
    {
        public override GameObject GetRandomPrefab(System.Random seed)
        {
            Item item = GetRandom(seed);

            if (item != null)
            {
                GameObject newItem = Instantiate(ItemUtility.GetPrefab(item));
                newItem.GetComponent<PickupableItem>().CreateItem(item);
                return newItem;
            }
            else
                return null;
        }
    }
}