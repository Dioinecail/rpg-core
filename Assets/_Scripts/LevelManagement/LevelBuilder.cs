﻿namespace RPG.LevelManagement
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using System.IO;
    using System.Text;
    using System.Linq;
    using System;

#if UNITY_EDITOR

    using UnityEditor;
    using RPG.Enemy;

#endif

    [Serializable]
    public class RoomTemplate
    {
        public char[,] roomTiles;


        public char this[int i, int j]
        {
            get { return roomTiles[i, j]; }
        }

        public RoomTemplate(List<char[]> tiles)
        {
            roomTiles = new char[tiles[0].Length, tiles.Count];

            for (int y = 0; y < tiles.Count; y++)
            {
                for (int x = 0; x < tiles[y].Length; x++)
                {
                    roomTiles[x, y] = tiles[y][x];
                }
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            for (int y = 0; y < roomTiles.GetLength(1); y++)
            {
                for (int x = 0; x < roomTiles.GetLength(0); x++)
                {
                    builder.Append(roomTiles[x, y]);
                }

                builder.AppendLine();
            }

            return builder.ToString();
        }
    }

    public class LevelBuilder : MonoBehaviour
    {
        public Action<Vector3> onLevelGeneratedEvent;

        public Transform levelRoot;
        public LevelBlocksAsset blocksAsset;
        public EnemyPackAsset enemyPackAsset;
        public LevelItemsAsset itemsAsset;

        public int gridSizeX, gridSizeY;
        public int roomSizeX, roomSizeY;
        public float blockSize;
        public string seed;

        public bool debugSolution;
        public bool debugLayout;
        public bool debugRooms;

        private RoomType[,] solution;
        private Dictionary<RoomType, List<RoomTemplate>> roomTemplatesDic;
        private RoomTemplate[,] layout;
        private System.Random seedObject;


        public void GenerateLevelLayout()
        {
            CreateLayout();
        }

        public void FillLevelLayout()
        {
            ClearLevel();
            Vector3 entrancePosition = CreateLevel();
            CreateBorder();
            onLevelGeneratedEvent?.Invoke(entrancePosition);
        }

        private void CreateLayout()
        {
            seedObject = new System.Random(seed.GetHashCode());
            LevelGenerator generator = new LevelGenerator(seedObject);
            solution = generator.GenerateSolution(gridSizeX, gridSizeY);
            roomTemplatesDic = generator.GetRoomTemplatesDictionary();
            layout = generator.GenerateLayout(solution, roomTemplatesDic);
        }

        private Vector3 CreateLevel()
        {
            Vector3 entrancePosition = Vector3.zero;

            for (int x = 0; x < layout.GetLength(0); x++)
            {
                for (int y = 0; y < layout.GetLength(1); y++)
                {
                    for (int j = 0; j < layout[x, y].roomTiles.GetLength(0); j++)
                    {
                        for (int k = 0; k < layout[x, y].roomTiles.GetLength(1); k++)
                        {
                            GameObject newBlock = null;

                            switch (layout[x, y][j, k])
                            {
                                case '1':
                                    // detect corner type
                                    TileType cType = DetectTileType(layout[x, y].roomTiles, j, k);
                                    // return corresponding corner based on corner type
                                    newBlock = blocksAsset.GetTile(cType, seedObject);
                                    newBlock.name = cType.ToString() + "_" + string.Format("[{0}:{1}]", x, y);
                                    break;
                                case '2':
                                    entrancePosition = new Vector2(j * blockSize, -k * blockSize) + new Vector2(x * roomSizeX * blockSize, -y * roomSizeY * blockSize);
                                    newBlock = blocksAsset.GetEntrance();
                                    newBlock.name = "Entrance_" + string.Format("[{0}:{1}]", x, y);
                                    break;
                                case '3':
                                    newBlock = blocksAsset.GetExit();
                                    newBlock.name = "Exit_" + string.Format("[{0}:{1}]", x, y);
                                    break;
                                case 'e':
                                    newBlock = enemyPackAsset.GetRandomPrefab(seedObject);
                                    break;
                                case 'i':
                                    newBlock = itemsAsset.GetRandomPrefab(seedObject);
                                    break;
                                default:
                                    break;
                            }

                            Vector2 roomPosition = new Vector2(x * roomSizeX * blockSize, -y * roomSizeY * blockSize);
                            Vector2 blockPosition = new Vector2(j * blockSize, -k * blockSize);

                            if (newBlock == null)
                                continue;

                            EnemyBase newEnemy = newBlock.GetComponent<EnemyBase>();

                            if (newEnemy != null)
                                newEnemy.Init();

                            newBlock.transform.SetParent(levelRoot);
                            newBlock.transform.position = roomPosition + blockPosition;
                        }
                    }
                }
            }

            return entrancePosition;
        }

        private void CreateBorder()
        {
            for (int x = 0; x < layout.GetLength(0); x++)
            {
                for (int j = 0; j < layout[x, 0].roomTiles.GetLength(0); j++)
                {
                    Vector2 roomPositionTop = new Vector2(x * roomSizeX * blockSize, 0);
                    Vector2 blockPositionTop = new Vector2(j * blockSize, blockSize);

                    CreateBlock(TileType.Wall, roomPositionTop + blockPositionTop);

                    Vector2 roomPositionBottom = new Vector2(x * roomSizeX * blockSize, -((layout.GetLength(0) - 1) * roomSizeY * blockSize));
                    Vector2 blockPositionBottom = new Vector2(j * blockSize, -layout[x, layout.GetLength(1) - 1].roomTiles.GetLength(0) * blockSize);

                    CreateBlock(TileType.Wall, roomPositionBottom + blockPositionBottom);
                }
            }

            for (int y = 0; y < layout.GetLength(1); y++)
            {
                for (int k = 0; k < layout[0, y].roomTiles.GetLength(1); k++)
                {
                    Vector2 roomPositionLeft = new Vector2(0, -y * roomSizeY * blockSize);
                    Vector2 blockPositionLeft = new Vector2(-blockSize, -k * blockSize);

                    CreateBlock(TileType.Wall, roomPositionLeft + blockPositionLeft);

                    Vector2 roomPositionRight = new Vector2(((layout.GetLength(1) - 1) * roomSizeX * blockSize), -y * roomSizeY * blockSize);
                    Vector2 blockPositionRight = new Vector2(layout[layout.GetLength(0) - 1, y].roomTiles.GetLength(1) * blockSize, -k * blockSize);

                    CreateBlock(TileType.Wall, roomPositionRight + blockPositionRight);
                }
            }
        }

        private void CreateBlock(TileType tileType, Vector3 position)
        {
            GameObject actualBlock = blocksAsset.GetTile(tileType, seedObject);
            actualBlock.transform.SetParent(levelRoot);
            actualBlock.transform.position = position;
            actualBlock.name = string.Format("{0}_[{1}:{2}]", tileType.ToString(), position.x.ToString("0.0"), position.y.ToString("0.0"));
        }

        private TileType DetectTileType(char[,] roomTiles, int j, int k)
        {
            TileType foundTileType = TileType.TopLeft;

            int wallsCount = 0;

            int sizeX = roomTiles.GetLength(0);
            int sizeY = roomTiles.GetLength(1);

            // check 4 wall direct wall sides
            bool topWall = !IsWithinRange(j, k - 1, sizeX, sizeY) || roomTiles[j, k - 1] == '1';
            bool leftWall = !IsWithinRange(j - 1, k, sizeX, sizeY) || roomTiles[j - 1, k] == '1';
            bool rightWall = !IsWithinRange(j + 1, k, sizeX, sizeY) || roomTiles[j + 1, k] == '1';
            bool bottomWall = !IsWithinRange(j, k + 1, sizeX, sizeY) || roomTiles[j, k + 1] == '1';

            if (topWall)
                wallsCount++;
            if (leftWall)
                wallsCount++;
            if (rightWall)
                wallsCount++;
            if (bottomWall)
                wallsCount++;

            switch (wallsCount)
            {
                case 0:
                    foundTileType = TileType.Platform;
                    break;
                case 1:
                    if (topWall)
                    {
                        foundTileType = TileType.Wall;
                    }
                    else
                    {
                        foundTileType = TileType.Platform;
                    }
                    break;
                case 2:
                    if (topWall && leftWall)
                    {
                        foundTileType = TileType.BottomRight;
                    }

                    if (topWall && rightWall)
                    {
                        foundTileType = TileType.BottomLeft;
                    }

                    if (bottomWall && leftWall)
                    {
                        foundTileType = TileType.TopRight;
                    }

                    if (bottomWall && rightWall)
                    {
                        foundTileType = TileType.TopLeft;
                    }

                    if (topWall && bottomWall)
                    {
                        foundTileType = TileType.Wall;
                    }

                    if (rightWall && leftWall)
                    {
                        foundTileType = TileType.Platform;
                    }
                    break;
                case 3:
                    // if top is empty then platform
                    // else wall
                    if (bottomWall)
                    {
                        foundTileType = TileType.Platform;
                    }
                    else
                    {
                        foundTileType = TileType.Wall;
                    }
                    break;
                case 4:
                    foundTileType = TileType.Wall;
                    break;
                default:

                    break;
            }

            return foundTileType;
        }

        private bool IsWithinRange(int x, int y, int maxX, int maxY)
        {
            bool isWithinRangeX = (x >= 0) && (x < maxX);
            bool isWIthinRangeY = (y >= 0) && (y < maxY);

            return isWithinRangeX && isWIthinRangeY;
        }

        private void ClearLevel()
        {
            int childCount = levelRoot.childCount;

            for (int i = 0; i < childCount; i++)
            {
                Destroy(levelRoot.GetChild(childCount - i - 1).gameObject);
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (solution == null || layout == null)
                return;

            Gizmos.color = Color.white;

            if (debugLayout)
            {
                for (int x = 0; x < layout.GetLength(0); x++)
                {
                    for (int y = 0; y < layout.GetLength(1); y++)
                    {
                        for (int j = 0; j < layout[x, y].roomTiles.GetLength(0); j++)
                        {
                            for (int k = 0; k < layout[x, y].roomTiles.GetLength(1); k++)
                            {
                                switch (layout[x, y][j, k])
                                {
                                    case '0':
                                        Gizmos.color = Color.white;
                                        break;
                                    case '1':
                                        Gizmos.color = Color.black;
                                        break;
                                    case '2':
                                        Gizmos.color = Color.blue;
                                        break;
                                    default:
                                        break;
                                }

                                Vector2 roomPosition = new Vector2(x * roomSizeX * blockSize, -y * roomSizeY * blockSize);

                                Vector2 blockPosition = new Vector2(j * blockSize, -k * blockSize);

                                Gizmos.DrawCube(roomPosition + blockPosition, Vector3.one * blockSize * 0.9f);
                            }
                        }
                    }
                }
            }

            if (debugSolution)
            {
                Vector2 offset = new Vector2(roomSizeX * blockSize * 0.5f, -roomSizeY * blockSize * 0.5f);

                for (int x = 0; x < layout.GetLength(0); x++)
                {
                    for (int y = 0; y < layout.GetLength(1); y++)
                    {
                        switch (solution[x, y])
                        {
                            case RoomType.Empty:
                                Gizmos.color = Color.white;
                                break;
                            case RoomType.LeftRight:
                                Gizmos.color = Color.red;
                                break;
                            case RoomType.LeftRightBottom:
                                Gizmos.color = Color.blue;
                                break;
                            case RoomType.LeftRightTop:
                                Gizmos.color = Color.green;
                                break;
                            case RoomType.Entrance:
                                Gizmos.color = Color.yellow;
                                break;
                            case RoomType.Exit:
                                Gizmos.color = Color.cyan;
                                break;
                            default:
                                break;
                        }

                        Gizmos.color = new Color(Gizmos.color.r, Gizmos.color.g, Gizmos.color.b, 0.5f);

                        Vector2 roomPosition = new Vector2(x * roomSizeX * blockSize, -y * roomSizeY * blockSize);
                        Gizmos.DrawCube(roomPosition + offset, new Vector3(roomSizeX * blockSize * 0.5f, roomSizeY * blockSize * 0.5f, 1));
                    }
                }
            }


            if (debugRooms)
            {
                if (roomTemplatesDic != null)
                {
                    int counter = 0;

                    foreach (var roomTemplates in roomTemplatesDic)
                    {
                        counter++;

                        Vector2 offsetY = new Vector2(0, counter + counter * 0.1f);

                        for (int i = 0; i < roomTemplates.Value.Count; i++)
                        {
                            Vector2 offset = new Vector2(i + 0.01f * i, 0) + offsetY;

                            for (int x = 0; x < roomTemplates.Value[i].roomTiles.GetLength(0); x++)
                            {
                                for (int y = 0; y < roomTemplates.Value[i].roomTiles.GetLength(1); y++)
                                {
                                    Vector2 position = new Vector2(x * 0.1f, -y * 0.1f) + offset;

                                    switch (roomTemplates.Value[i].roomTiles[x, y])
                                    {
                                        case '0':
                                            Gizmos.color = Color.white;
                                            break;
                                        case '1':
                                            Gizmos.color = Color.black;
                                            break;
                                        case '2':
                                            Gizmos.color = Color.blue;
                                            break;
                                        default:
                                            break;
                                    }

                                    Gizmos.DrawCube(position, Vector3.one * 0.09f);
                                }
                            }
                        }
                    }
                }
            }
        }
    }


#if UNITY_EDITOR

    [CustomEditor(typeof(LevelBuilder))]
    public class DEBUG_LevelGeneratorEditor : Editor
    {
        LevelBuilder generator;



        private void OnEnable()
        {
            generator = target as LevelBuilder;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Get New seed"))
                generator.seed = UnityEngine.Random.Range(0, 1000).ToString();

            if (GUILayout.Button("Generate level"))
                generator.GenerateLevelLayout();

            if (GUILayout.Button("Fill Level Layout"))
            {
                if (Application.isPlaying)
                    generator.FillLevelLayout();
            }
        }
    }

#endif

}