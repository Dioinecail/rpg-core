﻿namespace RPG.LevelManagement
{
    using RPG.Enemy;
    using UnityEngine;

    [CreateAssetMenu(menuName = "EnemyPackAsset/New", fileName = "EnemyPackAsset_")]
    public class EnemyPackAsset : LevelObjectsContainerAsset<EnemyBase>
    {

    }
}