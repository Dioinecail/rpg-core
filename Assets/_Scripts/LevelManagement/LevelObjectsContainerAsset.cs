﻿namespace RPG.LevelManagement
{
    using UnityEngine;
    using Utility;

    public class LevelObjectsContainerAsset<T> : ScriptableObject
    {
        [SerializeField] protected T[] possibleItems;



        public T GetRandom(System.Random seed)
        {
            return possibleItems.GetRandom(seed);
        }

        public virtual GameObject GetRandomPrefab(System.Random seed)
        {
            T item = GetRandom(seed);

            if (item != null)
            {
                if (item is MonoBehaviour)
                    return Instantiate((item as MonoBehaviour).gameObject);
                else
                    return null;
            }
            else
                return null;
        }
    }
}