﻿namespace RPG.LevelManagement
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using UnityEngine;
    using Utility;

    public enum RoomType
    {
        Empty = 0,
        LeftRight = 1,
        LeftRightBottom = 2,
        LeftRightTop = 3,
        Exit = 4,
        Entrance = 5
    }

    /// <summary>
    /// Generates a level layeout, then fills the layout with rooms, then fills the rooms with blocks
    /// </summary>
    public class LevelGenerator
    {
        public const string templatesAssetPath = "/_Prefabs/Level/RoomTemplates/RoomTemplate_";

        private System.Random rnd;



        public LevelGenerator(System.Random seed)
        {
            rnd = seed;
        }

        public RoomType[,] GenerateSolution(int gridSizeX, int gridSizeY)
        {
            RoomType[,] newSolution = new RoomType[gridSizeX, gridSizeY];

            // fill the whole grid with empty rooms
            for (int x = 0; x < gridSizeX; x++)
            {
                for (int y = 0; y < gridSizeY; y++)
                {
                    newSolution[x, y] = RoomType.Empty;
                }
            }

            // pick a random room in the top row
            int startingRoomIndex = rnd.Next(0, gridSizeX);

            // set the newly picked room to Entrance
            newSolution[startingRoomIndex, 0] = RoomType.Entrance;

            int currentRoomX = startingRoomIndex;
            int currentRoomY = 0;

            int lastRoomX = currentRoomX;
            int lastRoomY = currentRoomY;

            int currentDirection = 1;

            while (true)
            {
                int directionIndex = rnd.Next(1, 6);

                bool moveDown = false;

                if (directionIndex > 0 && directionIndex < 5)
                {
                    // go left or right
                    currentRoomX += currentDirection;

                    if (currentRoomX < 0 || currentRoomX > gridSizeX - 1)
                    {
                        currentRoomX -= currentDirection;
                        moveDown = true;
                    }
                    else
                    {
                        // mark new room as LeftRight
                        newSolution[currentRoomX, currentRoomY] = RoomType.LeftRight;

                        if (newSolution[lastRoomX, lastRoomY] == RoomType.LeftRightBottom)
                            newSolution[lastRoomX, lastRoomY] = RoomType.LeftRightTop;
                    }
                }
                else
                {
                    // go down
                    moveDown = true;
                }

                if (moveDown)
                {
                    currentDirection = -currentDirection;
                    currentRoomY++;

                    if (currentRoomY > gridSizeY - 1)
                    {
                        // we reached the end
                        // set the final room to Exit
                        newSolution[lastRoomX, lastRoomY] = RoomType.Exit;
                        break;
                    }

                    // mark last room as room LeftRightBottom
                    if (newSolution[lastRoomX, lastRoomY] != RoomType.Entrance)
                        newSolution[lastRoomX, lastRoomY] = RoomType.LeftRightBottom;
                    newSolution[currentRoomX, currentRoomY] = (RoomType)rnd.Next(2, 4);
                }

                lastRoomX = currentRoomX;
                lastRoomY = currentRoomY;
            }

            return newSolution;
        }

        public Dictionary<RoomType, List<RoomTemplate>> GetRoomTemplatesDictionary()
        {
            Dictionary<RoomType, List<RoomTemplate>> roomTemplatesDic = new Dictionary<RoomType, List<RoomTemplate>>();

            string[] roomTypes = Enum.GetNames(typeof(RoomType));

            foreach (string rType in roomTypes)
            {
                List<RoomTemplate> roomTemplates = GetRoomTemplates(rType);

                Enum.TryParse(rType, out RoomType roomType);

                roomTemplatesDic.Add(roomType, roomTemplates);
            }

            return roomTemplatesDic;
        }

        public RoomTemplate[,] GenerateLayout(RoomType[,] solution, Dictionary<RoomType, List<RoomTemplate>> templates)
        {
            RoomTemplate[,] layout = new RoomTemplate[solution.GetLength(0), solution.GetLength(1)];

            for (int x = 0; x < solution.GetLength(0); x++)
            {
                for (int y = 0; y < solution.GetLength(1); y++)
                {
                    RoomType rType = solution[x, y];

                    layout[x, y] = templates[rType].GetRandom(false, rnd);
                }
            }

            return layout;
        }

        private List<RoomTemplate> GetRoomTemplates(string rType)
        {
            List<RoomTemplate> templates = new List<RoomTemplate>();

            string path = Application.dataPath + templatesAssetPath + rType + ".csv";

            using (var stream = File.OpenRead(path))
            using (var parser = new CsvParser.CsvReader(stream, Encoding.UTF8, new CsvParser.CsvReader.Config() { WithQuotes = false }))
            {
                // Read CSV header
                if (!parser.MoveNext())
                    throw new InvalidDataException();

                int counter = 0;
                List<char[]> room = new List<char[]>();

                char[] row = new char[parser.Current.Count];

                for (int i = 0; i < parser.Current.Count; i++)
                {
                    row[i] = parser.Current[i][0];
                }

                room.Add(row);

                while (parser.MoveNext())
                {
                    counter++;
                    // Read CSV row data

                    row = new char[parser.Current.Count];

                    for (int i = 0; i < parser.Current.Count; i++)
                    {
                        row[i] = parser.Current[i][0];
                    }

                    if (counter % 10 == 0)
                    {
                        RoomTemplate newRoom = new RoomTemplate(room);
                        templates.Add(newRoom);
                        room.Clear();
                    }

                    room.Add(row);
                }
            }

            return templates;
        }

    }
}